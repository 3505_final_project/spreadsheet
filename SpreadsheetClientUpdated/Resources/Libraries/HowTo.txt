Good Day Sir! :)

Thank you for using this Spreadsheet program.

This program is designed by using two different modes:
"Spreadsheet Mode", and "Cell Modification Mode"

- In Spreadsheet Mode:
   - The user is only focused on/allowed to choosing a cell location by
   using the arrow keystrokes, UP, DOWN, RIGHT, and LEFT.

   - The user cannot modify the content of any cell. Only moving between
   cell locations.


- In Cell Modification Mode:
   - The user is only focused on/allowed to modify the contents text box
   in order to "register" those modifications into the selected cell.

   - The user cannot move to a different cell.


* Changing between the two modes (toggling):
- There are three ways that allows the user to toggle between the two modes:

   - 1_ Pressing the "Enter" key while in Spreadsheet mode, or Pressing 
   the "Escape" key while in "Cell Modification Mode" will allow the user
   to toggle between the modes.

   - 2_ Pressing the "Tab" Key will always allow the user to toggle between
   the modes.

   - 3_ Using the mouse to click on either the Cells or the Contents Text Box.

* The "Current Mode" text box will always show you which mode you are in.

* Use the "Enter" key while in "Cell Modification Mode" to register your 
modifications into the selected cell.

* This design decision was made to produce more secure and reliable program 
for the user to enjoy.

* Use the "reset" Button at any time to clear all Spreadsheet Contents in 
less than a second.

* Please click on the Features window to learn more...

* All rights reserved for:
Fahad A Alothaimeen
E-mail: uni_uk@hotmail.com