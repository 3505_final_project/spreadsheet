﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace SS
{
    /// <summary>
    /// Devoloper Partial Class for the Spreadsheet GUI
    /// </summary>
    /// <author>Fahad A Alothaimeen</author>
    /// <uNID>U0824563</uNID>
    public partial class SpreadsheetForm : Form
    {
        private AbstractSpreadsheet sheet;
        private SpreadsheetPanel ss;
        private int row, col;
        private bool isFrozen = true;
        private string value;
        private bool justOpened; // forces the "reSet" button to funtion properly
        private string fileName;

        public SpreadsheetForm()
        {
            InitializeComponent();
            spreadsheetPanel1.SelectionChanged += displaySelection;

            ss = this.spreadsheetPanel1;
            justOpened = false;
            CellNameBox.Text = "A1";
            ModeBox.Text = "Spreadsheet Mode";
        }


        /// <summary>
        /// Handles errors, only special error is error 1.
        /// </summary>
        /// <param name="errorCode"></param>
        /// <param name="message"></param>
        private void displayError(int errorCode, string message)
        {

            // TODO:  Logic shouldn't be handled in the view.
            if (errorCode == 1)
            {
                textBox2.Text = message;
                sheet.Undo();
            }
            if (errorCode == 4)
            {
                //TODO: Disallow the spreadsheet from displaying, should the username be invalid.
                textBox2.Text = message + " is already in use";

            }
            else
            {
                textBox2.Text = message;
            }
        }

        private void freezeClient()
        {
            isFrozen = true;
            textBox2.Text = "Loading...";
            
        }

        private void releaseClient()
        {
            if (isFrozen)
            {
                isFrozen = false;
            }
            textBox2.Text = "Done.";
        }

        /// <summary>
        /// Handles the mouse clicks on the Spreadsheet Panel
        /// </summary>
        /// <param name="ss">this Panel</param>
        private void displaySelection(SpreadsheetPanel ss)
        {
            // encode the cell Name
            ss.GetSelection(out col, out row);
            char c = Convert.ToChar(col + 65);
            string cellName = c.ToString() + (row + 1);

            ContentsBox.Text = sheet.GetCellContents(cellName).ToString();
            ValueTextBox.Text = sheet.GetCellValue(cellName).ToString();
            CellNameBox.Text = cellName;
            ActiveControl = spreadsheetPanel1;
            ModeBox.Text = "Spreadsheet Mode";
        }

        // ############################### Tool Strip Menu Items ############################## //

        /// <summary>
        /// A button to open a new Spreadsheet in a new window
        /// </summary>
        private void newToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            // Tell the application context to run the form on the same
            // thread as the other forms.
            DemoApplicationContext.getAppContext().RunForm(new SpreadsheetForm());
        }

        /// <summary>
        /// A button to open an existing Spreadsheet
        /// </summary>
        /*
        private void openToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            askToSave(sender, e);
            OpenFileDialog open = new OpenFileDialog();
            open.Filter = "Spreadsheet (*.sprd)|*.sprd|All Files(*.)|*.*";

            if (open.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    sheet = new Spreadsheet(open.FileName, s => true, s => s.ToUpper(), "ps6");
                    IEnumerable<string> cellNames = sheet.GetNamesOfAllNonemptyCells();
                    foreach (var cell in cellNames)
                    {
                        // Setting the cells to their correct contents/values
                        int rowCell;
                        Int32.TryParse(cell.Substring(1), out rowCell);
                        rowCell--;
                        int colCell = cell[0] - 65;
                        ss.SetValue(colCell, rowCell, sheet.GetCellValue(cell).ToString());
                    }

                    // Restore application status
                    ss.SetSelection(0, 0);
                    CellNameBox.Text = "A1";
                    ContentsBox.Text = sheet.GetCellContents("A1").ToString();
                    ValueTextBox.Text = sheet.GetCellValue("A1").ToString();
                    justOpened = true;
                }
                catch (Exception x)
                {
                    MessageBox.Show(x.Message);
                }
            }
        }
        */

        /// <summary>
        /// A Button to save the existing Spreadsheet
        /// </summary>
        private void saveToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (sheet.Changed)
            {
                if (fileName == null) // File was never saved
                {
                    saveProcessAs();
                }
                else
                {
                    sheet.Save(fileName);
                    MessageBox.Show("File was saved seccessfully");
                }
                justOpened = true;
            }
        }

        /// <summary>
        /// A Button to save the existing Spreadsheet in a specific location
        /// </summary>
        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveProcessAs();
        }

        /// <summary>
        /// A button to exit from the current Spreadsheet
        /// </summary>
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        /// <summary>
        /// A button to show a file that contains information on how to use this program
        /// </summary>
        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show(File.ReadAllText("./../../../Resources/Libraries/HowTo.txt"));
        }

        /// <summary>
        ///  A button to show a file that contains information on the added features
        /// </summary>
        private void featureToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show(File.ReadAllText("./../../../Resources/Libraries/Features.txt"));
        }

        // ########################### Buttons and Event Handlers Section ########################## //

        /// <summary>
        /// A button to clear all current Spreadsheet data
        /// </summary>
        private void ResetButton_Click(object sender, EventArgs e)
        {
            if (!isFrozen)
            {
                if (sheet.Changed || justOpened) // Only enable "reset" button when needed
                {
                    DialogResult dialogResult = MessageBox.Show("Are you sure you want to reset the current Spreadsheet?", "Resetting Spreadsheet", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {
                        IEnumerable<string> cellNamesList = sheet.GetNamesOfAllNonemptyCells();
                        sheet = new Spreadsheet(s => true, s => s.ToUpper(), "ps6");

                        foreach (string cell in cellNamesList)
                        {
                            // Setting the cells to their correct contents/values
                            int rowCell;
                            Int32.TryParse(cell.Substring(1), out rowCell);
                            rowCell--;
                            int colCell = cell[0] - 65;
                            ss.SetValue(colCell, rowCell, "");
                        }

                        // Restore application status
                        ContentsBox.Text = "";
                        ValueTextBox.Text = "";
                        ss.SetSelection(0, 0);
                        CellNameBox.Text = "A1";
                        justOpened = false;
                    }
                    if (ModeBox.Text == "Spreadsheet Mode")
                    {
                        ActiveControl = spreadsheetPanel1;
                    }
                    else { ActiveControl = ContentsBox; }
                } 
            }
        }

        /// <summary>
        /// A Spreadsheet "Closed" event Handler
        /// </summary>
        private void SpreadsheetForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (!isFrozen)
            {
                askToSave(sender, e);
                
            }
        }

        /// <summary>
        /// The contents box Keyboard Keys handler
        /// </summary>
        /// <param name="e">This source key</param>
        private void ContentsBox_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (!isFrozen)
            {
                if (e.KeyCode == Keys.Escape)
                {
                    // Helps to toggle between modes
                    ActiveControl = spreadsheetPanel1;
                    ModeBox.Text = "Spreadsheet Mode";
                }
                else if (e.KeyCode == Keys.Tab)
                {
                    // Helps to toggle between modes
                    ModeBox.Text = "Spreadsheet Mode";
                } 
            }
        }

        /// <summary>
        /// The contents box mouse clicks handler
        /// </summary>
        /// <param name="e">This source mouse click</param>
        private void ContentsBox_MouseClick(object sender, MouseEventArgs e)
        {
            if (!isFrozen)
            {
                // Helps to toggle between modes
                ModeBox.Text = "Cell Modification Mode"; 
            }
        }

        /// <summary>
        /// An event handler to hold/keep certain events to the controll
        /// </summary>
        /// <param name="keyData">This source Command key</param>
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (!isFrozen)
            {
                if (this.ActiveControl.Name == ContentsBox.Name)
                {
                    // The "Enter" key is captured here to prevent "TextBox" from producing
                    // newLine characters.
                    if (keyData == Keys.Enter)
                    {
                        value = ContentsBox.Text;
                        ss.GetSelection(out col, out row);

                        char c = Convert.ToChar(col + 65);
                        string cellName = c.ToString() + (row + 1);
                        try
                        {
                            //ISet<string> cellNames = sheet.SetContentsOfCell(cellName, value);

                            // send a command to the server to approve the change request 
                            sheet.send_cmd(cellName, value);
                            /*
                            foreach (var cell in cellNames)
                            {
                                // Setting the cells to their correct contents/values
                                int rowCell;
                                Int32.TryParse(cell.Substring(1), out rowCell);
                                rowCell--;
                                int colCell = cell[0] - 65;
                                ss.SetValue(colCell, rowCell, sheet.GetCellValue(cell).ToString());
                            }*/

                            // Restore application status
                            if ((row + 1) <= 98)
                            {
                                ss.SetSelection(col, row + 1);
                                cellName = c.ToString() + ((row + 1) + 1);
                            }
                            syncTextBoxesValues(cellName);
                        }
                        catch (Exception x)
                        {
                            MessageBox.Show(x.Message);
                        }
                        return true;
                    }
                    return false;
                }
                else if (keyData == Keys.Down)
                {
                    ss.GetSelection(out col, out row);
                    row++;
                    if (row <= 98) { moveSelection(); }
                    return true;
                }
                else if (keyData == Keys.Right)
                {
                    ss.GetSelection(out col, out row);
                    col++;
                    if (col <= 25) { moveSelection(); }
                    return true;
                }
                else if (keyData == Keys.Left)
                {
                    ss.GetSelection(out col, out row);
                    col--;
                    if (col >= 0) { moveSelection(); }
                    return true;
                }
                else if (keyData == Keys.Up)
                {
                    ss.GetSelection(out col, out row);
                    row--;
                    if (row >= 0) { moveSelection(); }
                    return true;
                }
                else if (keyData == Keys.Enter)
                {
                    // Helps to toggle between modes
                    ActiveControl = ContentsBox;
                    ModeBox.Text = "Cell Modification Mode";
                }
                else if (keyData == Keys.Tab)
                {
                    // Helps to toggle between modes
                    ModeBox.Text = "Cell Modification Mode";
                }
                return base.ProcessCmdKey(ref msg, keyData); 
            }

            return false;
        }

        // ############################### Helper Methods Section ############################### //

        /// <summary>
        /// A private helper method to save a File to a specific location
        /// </summary>
        private void saveProcessAs()
        {
            SaveFileDialog save = new SaveFileDialog();
            save.Filter = "Spreadsheet (*.sprd)|*.sprd|All Files(*.)|*.*";

            if (save.ShowDialog() == DialogResult.OK)
            {
                fileName = save.FileName;
                sheet.Save(fileName);
            }
        }

        /// <summary>
        /// A private helper method that move the cell selection box
        /// </summary>
        private void moveSelection()
        {
            ss.SetSelection(col, row);

            // encode the cell Name
            char c = Convert.ToChar(col + 65);
            string cellName = c.ToString() + (row + 1);
            syncTextBoxesValues(cellName);
        }

        /// <summary>
        /// A private helper method to ask the user to save unsaved work
        /// </summary>
        private void askToSave(object sender, EventArgs e)
        {
            /**
            if (sheet.Changed)
            {
                DialogResult dialogResult = MessageBox.Show("You did not save your Spreadsheet, do you want to save it?", "Unsaved Work!", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    saveToolStripMenuItem1_Click(sender, e);
                }
            }
            **/
        }

        /// <summary>
        /// A private helper method to sync the text boxes with the current status of the
        /// underling Spreadsheet data
        /// </summary>
        /// <param name="cellName">the name of the cell that was modified</param>
        private void syncTextBoxesValues(string cellName)
        {
            // To avoid making unnecesarry cells, we need to produce a cell when it exsits
            // else, just show am empty strings
            IEnumerable<string> cellNames = sheet.GetNamesOfAllNonemptyCells();
            if (cellNames.Contains<string>(cellName))
            {
                ContentsBox.Text = sheet.GetCellContents(cellName).ToString();
                ValueTextBox.Text = sheet.GetCellValue(cellName).ToString();
            }
            else
            {
                ContentsBox.Text = "";
                ValueTextBox.Text = "";
            }
            CellNameBox.Text = cellName;
        }

        private void CellNameBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void undoToolStripMenuItem_Click(object sender, EventArgs e) {
            sheet.Undo();
        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void loginBtn_Click(object sender, EventArgs e) {
            System.Diagnostics.Debug.WriteLine("Login Buttong was clicked");
            sheet = new Spreadsheet(hostnameText.Text, int.Parse(portText.Text), 
                                    usernameText.Text, spreadsheetNameText.Text, 
                                    this);
            sheet.viewError += displayError;
            sheet.freeze += freezeClient;
            sheet.release += releaseClient;
            sheet.updateCells += beginUpdateView;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cellNames"></param>
        private void beginUpdateView(ISet<string> cellNames)
        {
            foreach (var cell in cellNames)
            {
                // Setting the cells to their correct contents/values
                int rowCell;
                Int32.TryParse(cell.Substring(1), out rowCell);
                rowCell--;
                int colCell = cell[0] - 65;
                ss.Invoke(new Action(() => { ss.SetValue(colCell, rowCell, sheet.GetCellValue(cell).ToString()); }));
            }
        }
    }
}
