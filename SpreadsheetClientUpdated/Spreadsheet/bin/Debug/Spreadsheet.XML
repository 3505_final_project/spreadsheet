<?xml version="1.0"?>
<doc>
    <assembly>
        <name>Spreadsheet</name>
    </assembly>
    <members>
        <member name="T:SS.CircularException">
            <summary>
            Thrown to indicate that a change to a cell will cause a circular dependency.
            </summary>
        </member>
        <member name="T:SS.InvalidNameException">
            <summary>
            Thrown to indicate that a name parameter was either null or invalid.
            </summary>
        </member>
        <member name="T:SS.SpreadsheetReadWriteException">
            <summary>
            Thrown to indicate that a read or write attempt has failed.
            </summary>
        </member>
        <member name="M:SS.SpreadsheetReadWriteException.#ctor(System.String)">
            <summary>
            Creates the exception with a message
            </summary>
        </member>
        <member name="T:SS.AbstractSpreadsheet">
            <summary>
            An AbstractSpreadsheet object represents the state of a simple spreadsheet.  A 
            spreadsheet consists of an infinite number of named cells.
            
            A string is a cell name if and only if it consists of one or more letters,
            followed by one or more digits AND it satisfies the predicate IsValid.
            For example, "A15", "a15", "XY032", and "BC7" are cell names so long as they
            satisfy IsValid.  On the other hand, "Z", "X_", and "hello" are not cell names,
            regardless of IsValid.
            
            Any valid incoming cell name, whether passed as a parameter or embedded in a formula,
            must be normalized with the Normalize method before it is used by or saved in 
            this spreadsheet.  For example, if Normalize is s => s.ToUpper(), then
            the Formula "x3+a5" should be converted to "X3+A5" before use.
            
            A spreadsheet contains a cell corresponding to every possible cell name.  
            In addition to a name, each cell has a contents and a value.  The distinction is
            important.
            
            The contents of a cell can be (1) a string, (2) a double, or (3) a Formula.  If the
            contents is an empty string, we say that the cell is empty.  (By analogy, the contents
            of a cell in Excel is what is displayed on the editing line when the cell is selected.)
            
            In a new spreadsheet, the contents of every cell is the empty string.
             
            The value of a cell can be (1) a string, (2) a double, or (3) a FormulaError.  
            (By analogy, the value of an Excel cell is what is displayed in that cell's position
            in the grid.)
            
            If a cell's contents is a string, its value is that string.
            
            If a cell's contents is a double, its value is that double.
            
            If a cell's contents is a Formula, its value is either a double or a FormulaError,
            as reported by the Evaluate method of the Formula class.  The value of a Formula,
            of course, can depend on the values of variables.  The value of a variable is the 
            value of the spreadsheet cell it names (if that cell's value is a double) or 
            is undefined (otherwise).
            
            Spreadsheets are never allowed to contain a combination of Formulas that establish
            a circular dependency.  A circular dependency exists when a cell depends on itself.
            For example, suppose that A1 contains B1*2, B1 contains C1*2, and C1 contains A1*2.
            A1 depends on B1, which depends on C1, which depends on A1.  That's a circular
            dependency.
            </summary>
        </member>
        <member name="F:SS.AbstractSpreadsheet.theGui">
            <summary>
            This object is ver important.  Use it to send messages to the gui.
            An example is:  theGui.BeginInvoke(viewError, id, msg);
            The above is used to call a Gui method called viewError with parameters id and msg
            It is equivalent to viewError(id, msg); The reason you can't do viewError(id, msg) is
            that MS Windows says that only the gui thread can do stuff to the GUI thread
            Invoke asks the gui thread to viewError with parameters id and msg whenever 
            it gets around to it
            </summary>
        </member>
        <member name="M:SS.AbstractSpreadsheet.#ctor(System.Func{System.String,System.Boolean},System.Func{System.String,System.String},System.String)">
            <summary>
            Constructs an abstract spreadsheet by recording its variable validity test,
            its normalization method, and its version information.  The variable validity
            test is used throughout to determine whether a string that consists of one or
            more letters followed by one or more digits is a valid cell name.  The variable
            equality test should be used thoughout to determine whether two variables are
            equal.
            </summary>
        </member>
        <member name="M:SS.AbstractSpreadsheet.GetSavedVersion(System.String)">
            <summary>
            Returns the version information of the spreadsheet saved in the named file.
            If there are any problems opening, reading, or closing the file, the method
            should throw a SpreadsheetReadWriteException with an explanatory message.
            </summary>
        </member>
        <member name="M:SS.AbstractSpreadsheet.Save(System.String)">
            <summary>
            Writes the contents of this spreadsheet to the named file using an XML format.
            The XML elements should be structured as follows:
            
            <spreadsheet version="version information goes here">
            
            <cell>
            <name>
            cell name goes here
            </name>
            <contents>
            cell contents goes here
            </contents>    
            </cell>
            
            </spreadsheet>
            
            There should be one cell element for each non-empty cell in the spreadsheet.  
            If the cell contains a string, it should be written as the contents.  
            If the cell contains a double d, d.ToString() should be written as the contents.  
            If the cell contains a Formula f, f.ToString() with "=" prepended should be written as the contents.
            
            If there are any problems opening, writing, or closing the file, the method should throw a
            SpreadsheetReadWriteException with an explanatory message.
            </summary>
        </member>
        <member name="M:SS.AbstractSpreadsheet.GetCellValue(System.String)">
            <summary>
            If name is null or invalid, throws an InvalidNameException.
            
            Otherwise, returns the value (as opposed to the contents) of the named cell.  The return
            value should be either a string, a double, or a SpreadsheetUtilities.FormulaError.
            </summary>
        </member>
        <member name="M:SS.AbstractSpreadsheet.GetNamesOfAllNonemptyCells">
            <summary>
            Enumerates the names of all the non-empty cells in the spreadsheet.
            </summary>
        </member>
        <member name="M:SS.AbstractSpreadsheet.GetCellContents(System.String)">
            <summary>
            If name is null or invalid, throws an InvalidNameException.
            
            Otherwise, returns the contents (as opposed to the value) of the named cell.  The return
            value should be either a string, a double, or a Formula.
            </summary>
        </member>
        <member name="M:SS.AbstractSpreadsheet.SetContentsOfCell(System.String,System.String)">
            <summary>
            If content is null, throws an ArgumentNullException.
            
            Otherwise, if name is null or invalid, throws an InvalidNameException.
            
            Otherwise, if content parses as a double, the contents of the named
            cell becomes that double.
            
            Otherwise, if content begins with the character '=', an attempt is made
            to parse the remainder of content into a Formula f using the Formula
            constructor.  There are then three possibilities:
            
              (1) If the remainder of content cannot be parsed into a Formula, a 
                  SpreadsheetUtilities.FormulaFormatException is thrown.
                  
              (2) Otherwise, if changing the contents of the named cell to be f
                  would cause a circular dependency, a CircularException is thrown.
                  
              (3) Otherwise, the contents of the named cell becomes f.
            
            Otherwise, the contents of the named cell becomes content.
            
            If an exception is not thrown, the method returns a set consisting of
            name plus the names of all other cells whose value depends, directly
            or indirectly, on the named cell.
            
            For example, if name is A1, B1 contains A1*2, and C1 contains B1+A1, the
            set {A1, B1, C1} is returned.
            </summary>
        </member>
        <member name="M:SS.AbstractSpreadsheet.Undo">
            <summary>
            Reverts the last edit to the spreadsheet.
            </summary>
        </member>
        <member name="M:SS.AbstractSpreadsheet.SetCellContents(System.String,System.Double)">
            <summary>
            If name is null or invalid, throws an InvalidNameException.
            
            Otherwise, the contents of the named cell becomes number.  The method returns a
            set consisting of name plus the names of all other cells whose value depends, 
            directly or indirectly, on the named cell.
            
            For example, if name is A1, B1 contains A1*2, and C1 contains B1+A1, the
            set {A1, B1, C1} is returned.
            </summary>
        </member>
        <member name="M:SS.AbstractSpreadsheet.SetCellContents(System.String,System.String)">
            <summary>
            If text is null, throws an ArgumentNullException.
            
            Otherwise, if name is null or invalid, throws an InvalidNameException.
            
            Otherwise, the contents of the named cell becomes text.  The method returns a
            set consisting of name plus the names of all other cells whose value depends, 
            directly or indirectly, on the named cell.
            
            For example, if name is A1, B1 contains A1*2, and C1 contains B1+A1, the
            set {A1, B1, C1} is returned.
            </summary>
        </member>
        <member name="M:SS.AbstractSpreadsheet.SetCellContents(System.String,SpreadsheetUtilities.Formula)">
            <summary>
            If formula parameter is null, throws an ArgumentNullException.
            
            Otherwise, if name is null or invalid, throws an InvalidNameException.
            
            Otherwise, if changing the contents of the named cell to be the formula would cause a 
            circular dependency, throws a CircularException.
            
            Otherwise, the contents of the named cell becomes formula.  The method returns a
            Set consisting of name plus the names of all other cells whose value depends,
            directly or indirectly, on the named cell.
            
            For example, if name is A1, B1 contains A1*2, and C1 contains B1+A1, the
            set {A1, B1, C1} is returned.
            </summary>
        </member>
        <member name="M:SS.AbstractSpreadsheet.GetDirectDependents(System.String)">
            <summary>
            If name is null, throws an ArgumentNullException.
            
            Otherwise, if name isn't a valid cell name, throws an InvalidNameException.
            
            Otherwise, returns an enumeration, without duplicates, of the names of all cells whose
            values depend directly on the value of the named cell.  In other words, returns
            an enumeration, without duplicates, of the names of all cells that contain
            formulas containing name.
            
            For example, suppose that
            A1 contains 3
            B1 contains the formula A1 * A1
            C1 contains the formula B1 + A1
            D1 contains the formula B1 - C1
            The direct dependents of A1 are B1 and C1
            </summary>
        </member>
        <member name="M:SS.AbstractSpreadsheet.send_cmd(System.String,System.String)">
            <summary>
            
            </summary>
            <param name="cellNames"></param>
        </member>
        <member name="M:SS.AbstractSpreadsheet.GetCellsToRecalculate(System.Collections.Generic.ISet{System.String})">
            <summary>
            Requires that names be non-null.  Also requires that if names contains s,
            then s must be a valid non-null cell name.
            
            If any of the named cells are involved in a circular dependency,
            throws a CircularException.
            
            Otherwise, returns an enumeration of the names of all cells whose values must
            be recalculated, assuming that the contents of each cell named in names has changed.
            The names are enumerated in the order in which the calculations should be done.  
            
            For example, suppose that 
            A1 contains 5
            B1 contains 7
            C1 contains the formula A1 + B1
            D1 contains the formula A1 * C1
            E1 contains 15
            
            If A1 and B1 have changed, then A1, B1, and C1, and D1 must be recalculated,
            and they must be recalculated in either the order A1,B1,C1,D1 or B1,A1,C1,D1.
            The method will produce one of those enumerations.
            
            Please note that this method depends on the abstract GetDirectDependents.
            It won't work until GetDirectDependents is implemented correctly.
            </summary>
        </member>
        <member name="M:SS.AbstractSpreadsheet.GetCellsToRecalculate(System.String)">
            <summary>
            A convenience method for invoking the other version of GetCellsToRecalculate
            with a singleton set of names.  See the other version for details.
            </summary>
        </member>
        <member name="M:SS.AbstractSpreadsheet.Visit(System.String,System.String,System.Collections.Generic.ISet{System.String},System.Collections.Generic.LinkedList{System.String})">
            <summary>
            A helper for the GetCellsToRecalculate method.
            </summary>
        </member>
        <member name="P:SS.AbstractSpreadsheet.Changed">
            <summary>
            True if this spreadsheet has been modified since it was created or saved                  
            (whichever happened most recently); false otherwise.
            </summary>
        </member>
        <member name="P:SS.AbstractSpreadsheet.IsValid">
            <summary>
            Method used to determine whether a string that consists of one or more letters
            followed by one or more digits is a valid variable name.
            </summary>
        </member>
        <member name="P:SS.AbstractSpreadsheet.Normalize">
            <summary>
            Method used to convert a cell name to its standard form.  For example,
            Normalize might convert names to upper case.
            </summary>
        </member>
        <member name="P:SS.AbstractSpreadsheet.Version">
            <summary>
            Version information
            </summary>
        </member>
        <member name="T:SS.Cell">
            <summary>
            A convenient class to represent a cell object
            </summary>
            <modifier>Fahad A Alothaimeen</modifier>
            <uNID>U0824563</uNID>
        </member>
        <member name="M:SS.Cell.#ctor(System.String)">
            <summary>
            Instantiate a new string cell
            </summary>
            <param name="_content">
            The string that will be created in the cell
            </param>
        </member>
        <member name="M:SS.Cell.#ctor(System.Double)">
            <summary>
            Instantiate a new double cell
            </summary>
            <param name="_content">
            The double that will be created in the cell
            </param>
        </member>
        <member name="M:SS.Cell.#ctor(SpreadsheetUtilities.Formula,System.Collections.Generic.Dictionary{System.String,SS.Cell})">
            <summary>
            Instantiate a new formula cell
            </summary>
            <param name="_content">
            The formula that will be created in the cell
            </param>
            <param name="Cells">
            The collection of the cells so far
            </param>
        </member>
        <member name="M:SS.Cell.testerMethod(System.String)">
            <summary>
            provides a convenient way to get the cells values so far 
            when a new formula cell is instantiated/requested
            </summary>
            <param name="cell">The name of the cell</param>
            <returns>The value of a given name of cell</returns>
        </member>
        <member name="M:SS.Cell.getCellContent">
            <summary>
            Returns the a content of a cell
            </summary>
            <returns>The content of this cell</returns>
        </member>
        <member name="M:SS.Cell.getCellValue">
            <summary>
            Returns the a value of a cell
            </summary>
            <returns>The value of this cell</returns>
        </member>
        <member name="M:SS.Cell.isCellEmpty">
            <summary>
            Returns true, if the cell is empty
            false, otherwise
            </summary>
            <returns>true, if this cell is empty</returns>
        </member>
        <member name="M:Spreadsheet.Communicator.MessageReceived(System.String,System.Exception,System.Object)">
            <summary>
            Deals with all messages from the Server to the Client
            </summary>
            <param name="msg"></param>
            <param name="e"></param>
            <param name="payload"></param>
        </member>
        <member name="E:Spreadsheet.Communicator.ConfirmConn">
            <summary>
            Sent to the Spreadsheet with the number of cells that it should expect
            </summary>
        </member>
        <member name="T:SS.Spreadsheet">
            <summary>
            A class that represents a Spreedsheet with cells
            </summary>
            <modifier>Fahad A Alothaimeen</modifier>
            <uNID>U0824563</uNID>
        </member>
        <member name="F:SS.Spreadsheet.Cells">
            <summary>
            This is a dictinary that represent a spreadsheet,
            A spreadsheet is a collection of cells that could
            depend on each other.
            
            A cell name is the kay, and the cell object is the value
            </summary>
        </member>
        <member name="F:SS.Spreadsheet.Graph">
            <summary>
            Dpendency Graph to represent the 
            dependencies between cells if any
            </summary>
        </member>
        <member name="F:SS.Spreadsheet.serverComm">
            <summary>
            Object that sends and receives all messages to and from the server
            </summary>
        </member>
        <member name="F:SS.Spreadsheet.expectedCells">
            <summary>
            Expected number of cells to be received by the server.
            </summary>
        </member>
        <member name="M:SS.Spreadsheet.#ctor">
            <summary>
            Creats an empty Spreadsheet
            </summary>
        </member>
        <member name="M:SS.Spreadsheet.#ctor(System.Func{System.String,System.Boolean},System.Func{System.String,System.String},System.String)">
            <summary>
            Constructs an override spreadsheet by recording its variable validity test,
            its normalization method, and its version information.  The variable validity
            test is used throughout to determine whether a string that consists of one or
            more letters followed by one or more digits is a valid cell name.  The variable
            equality test should be used thoughout to determine whether two variables are
            equal.
            </summary>
        </member>
        <member name="M:SS.Spreadsheet.#ctor(System.String,System.Int32,System.String,System.String,System.Windows.Forms.Form)">
            Constructor to connect the spreadsheet to a server.  
            This is the only constructor that initializes the connection to the server
            so only this constructor can be used now.
        </member>
        <member name="M:SS.Spreadsheet.SetClientCellHelper(System.String,System.String)">
            <summary>
            Event handler for setting the client cell.
            </summary>
            <param name="name"></param>
            <param name="content"></param>
        </member>
        <member name="M:SS.Spreadsheet.ConfirmConnectionHelper(System.Int32)">
            <summary>
            Event handler for confirming client connection.
            The number of cells is sent from the server.
            The client will freeze to prevent edits until it has received 
            that many cells from the server.
            </summary>
            <param name="value">The number of cells to expect from the server</param>
        </member>
        <member name="M:SS.Spreadsheet.ErrorHelper(System.Int32,System.String)">
            <summary>
            Event handler that handles errors.
            </summary>
            <param name="errorCode"></param>
            <param name="message"></param>
        </member>
        <member name="M:SS.Spreadsheet.GetSavedVersion(System.String)">
            <summary>
            Returns the version information of the spreadsheet saved in the named file.
            If there are any problems opening, reading, or closing the file, the method
            should throw a SpreadsheetReadWriteException with an explanatory message.
            </summary>
        </member>
        <member name="M:SS.Spreadsheet.Save(System.String)">
            <summary>
            Writes the contents of this spreadsheet to the named file using an XML format.
            The XML elements should be structured as follows:
            
            <spreadsheet version="version information goes here">
            
            <cell>
            <name>
            cell name goes here
            </name>
            <contents>
            cell contents goes here
            </contents>    
            </cell>
            
            </spreadsheet>
            
            There should be one cell element for each non-empty cell in the spreadsheet.  
            If the cell contains a string, it should be written as the contents.  
            If the cell contains a double d, d.ToString() should be written as the contents.  
            If the cell contains a Formula f, f.ToString() with "=" prepended should be written as the contents.
            
            If there are any problems opening, writing, or closing the file, the method should throw a
            SpreadsheetReadWriteException with an explanatory message.
            </summary>
        </member>
        <member name="M:SS.Spreadsheet.GetCellValue(System.String)">
            <summary>
            If name is null or invalid, throws an InvalidNameException.
            
            Otherwise, returns the value (as opposed to the contents) of the named cell.  The return
            value should be either a string, a double, or a SpreadsheetUtilities.FormulaError.
            </summary>
        </member>
        <member name="M:SS.Spreadsheet.GetNamesOfAllNonemptyCells">
            <summary>
            Enumerates the names of all the non-empty cells in the spreadsheet.
            </summary>
        </member>
        <member name="M:SS.Spreadsheet.GetCellContents(System.String)">
            <summary>
            If name is null or invalid, throws an InvalidNameException.
            
            Otherwise, returns the contents (as opposed to the value) of the named cell.  The return
            value should be either a string, a double, or a Formula.
            </summary>
        </member>
        <member name="M:SS.Spreadsheet.send_cmd(System.String,System.String)">
            <summary>
            
            </summary>
            <param name="name"></param>
            <param name="content"></param>
        </member>
        <member name="M:SS.Spreadsheet.SetContentsOfCell(System.String,System.String)">
            <summary>
            If content is null, throws an ArgumentNullException.
            
            Otherwise, if name is null or invalid, throws an InvalidNameException.
            
            Otherwise, if content parses as a double, the contents of the named
            cell becomes that double.
            
            Otherwise, if content begins with the character '=', an attempt is made
            to parse the remainder of content into a Formula f using the Formula
            constructor.  There are then three possibilities:
            
              (1) If the remainder of content cannot be parsed into a Formula, a 
                  SpreadsheetUtilities.FormulaFormatException is thrown.
                  
              (2) Otherwise, if changing the contents of the named cell to be f
                  would cause a circular dependency, a CircularException is thrown.
                  
              (3) Otherwise, the contents of the named cell becomes f.
            
            Otherwise, the contents of the named cell becomes content.
            
            If an exception is not thrown, the method returns a set consisting of
            name plus the names of all other cells whose value depends, directly
            or indirectly, on the named cell.
            
            For example, if name is A1, B1 contains A1*2, and C1 contains B1+A1, the
            set {A1, B1, C1} is returned.
            </summary>
        </member>
        <member name="M:SS.Spreadsheet.Undo">
            Reverts the last cell edit made to the spreadsheet.
        </member>
        <member name="M:SS.Spreadsheet.SetCellContents(System.String,System.Double)">
            <summary>
            If name is null or invalid, throws an InvalidNameException.
            
            Otherwise, the contents of the named cell becomes number.  The method returns a
            set consisting of name plus the names of all other cells whose value depends, 
            directly or indirectly, on the named cell.
            
            For example, if name is A1, B1 contains A1*2, and C1 contains B1+A1, the
            set {A1, B1, C1} is returned.
            </summary>
        </member>
        <member name="M:SS.Spreadsheet.SetCellContents(System.String,System.String)">
            <summary>
            If text is null, throws an ArgumentNullException.
            
            Otherwise, if name is null or invalid, throws an InvalidNameException.
            
            Otherwise, the contents of the named cell becomes text.  The method returns a
            set consisting of name plus the names of all other cells whose value depends, 
            directly or indirectly, on the named cell.
            
            For example, if name is A1, B1 contains A1*2, and C1 contains B1+A1, the
            set {A1, B1, C1} is returned.
            </summary>
        </member>
        <member name="M:SS.Spreadsheet.SetCellContents(System.String,SpreadsheetUtilities.Formula)">
            <summary>
            If formula parameter is null, throws an ArgumentNullException.
            
            Otherwise, if name is null or invalid, throws an InvalidNameException.
            
            Otherwise, if changing the contents of the named cell to be the formula would cause a 
            circular dependency, throws a CircularException.
            
            Otherwise, the contents of the named cell becomes formula.  The method returns a
            Set consisting of name plus the names of all other cells whose value depends,
            directly or indirectly, on the named cell.
            
            For example, if name is A1, B1 contains A1*2, and C1 contains B1+A1, the
            set {A1, B1, C1} is returned.
            </summary>
        </member>
        <member name="M:SS.Spreadsheet.GetDirectDependents(System.String)">
            <summary>
            If name is null, throws an ArgumentNullException.
            
            Otherwise, if name isn't a valid cell name, throws an InvalidNameException.
            
            Otherwise, returns an enumeration, without duplicates, of the names of all cells whose
            values depend directly on the value of the named cell.  In other words, returns
            an enumeration, without duplicates, of the names of all cells that contain
            formulas containing name.
            
            For example, suppose that
            A1 contains 3
            B1 contains the formula A1 * A1
            C1 contains the formula B1 + A1
            D1 contains the formula B1 - C1
            The direct dependents of A1 are B1 and C1
            </summary>
        </member>
        <member name="M:SS.Spreadsheet.Recalculate(System.Collections.Generic.ISet{System.String})">
            <summary>
            Takes in a Set of Formula cells that need to be reCalculated, 
            and reConstruct them
            </summary>
            <param name="Set">The Set of formulas to be reCalculated</param>
        </member>
        <member name="M:SS.Spreadsheet.versionChecking(System.String)">
            <summary>
            Check the version attribute of the given XML file through the file Path
            </summary>
            <param name="filePath">The path for the XML file</param>
            <returns>The version attribute</returns>
        </member>
        <member name="P:SS.Spreadsheet.Changed">
            <summary>
            True if this spreadsheet has been modified since it was created or saved                  
            (whichever happened most recently); false otherwise.
            </summary>
        </member>
        <member name="T:SS.StringSocket">
            <summary>
            A Wrapper class for the Socket class that uses strings instead of bytes
            </summary>
            <Assignment> PS10 </Assignment>
            <Authors> Sam Godfrey and Fahad Alothaimeen </Authors>
        </member>
        <member name="M:SS.StringSocket.#ctor(System.Net.Sockets.Socket,System.Text.Encoding)">
            <summary>
            The constructor for our string socket class
            </summary>
            <param name="s">Connected socket to be used throughout the string socket to send and receive messages</param>
            <param name="e">The encoding style to be used when we are building strings from bytes</param>
        </member>
        <member name="M:SS.StringSocket.BeginSend(System.String,SS.StringSocket.SendCallback,System.Object)">
            <summary>
            The begin send method that takes in a string, changes it to a byte array and then uses the underlying socket to send a message
            </summary>
            <param name="s">String to send</param>
            <param name="callback">Callback method to be called after the message has been sent</param>
            <param name="payload">Arbitrary object that can be used as anything, identifier or whatever</param>
        </member>
        <member name="M:SS.StringSocket.SendBytes">
            <summary>
            Sends the bytes to the endPoint destination
            </summary>
        </member>
        <member name="M:SS.StringSocket.MessageSent(System.IAsyncResult)">
            <summary>
            The callback that is called upon completion of a socket.beginsend method, this is not the callback that we pass into the stringsocket.beginsend method
            </summary>
            <param name="result">The result of the socket.beginsend method</param>
        </member>
        <member name="M:SS.StringSocket.BeginReceive(SS.StringSocket.ReceiveCallback,System.Object)">
            <summary>
            The method that calls BeginReceive with the underlying socket. We queue the parameters so we can use them later in the BeginRecieve callback
            </summary>
            <param name="callback">Callback method to be called during the BeginReceive callback</param>
            <param name="payload">Arbitrary object that can be used as anything, identifier or whatever</param>
        </member>
        <member name="M:SS.StringSocket.MessageRecieved(System.IAsyncResult)">
            <summary>
            The message received call back, this method checks to make sure that we have received all of the bytes that were sent, or that we received 0 bytes
            </summary>
            <param name="ar">The result of the socket.beginreceive method</param>
        </member>
        <member name="M:SS.StringSocket.Close">
            <summary>
            First shutsdown the underlying socket and then closes down the socket
            </summary>
        </member>
        <member name="T:SS.StringSocket.SendCallback">
            <summary>
            DescribeS the callbacks that are used for sending strings.
            </summary>
            <param name="e">Exception</param>
            <param name="payload">Payload Object</param>
        </member>
        <member name="T:SS.StringSocket.ReceiveCallback">
            <summary>
            Describe the callbacks that are used for receiving strings.
            </summary>
            <param name="s">Message Recieved</param>
            <param name="e">Exception</param>
            <param name="payload">Payload Object</param>
        </member>
        <member name="T:SS.StringSocket.CallBacksInfo">
            <summary>
            A structure that holds all of the info for the Message Receive call back, such as payload and the callback function
            </summary>
        </member>
    </members>
</doc>
