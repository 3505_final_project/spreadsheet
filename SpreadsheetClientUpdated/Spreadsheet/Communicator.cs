﻿using SS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Spreadsheet
{
    class Communicator
    {
        // TODO:
        private TcpClient client;
        private StringSocket serverConn;
        /// <summary>
        /// Sent to the Spreadsheet with the number of cells that it should expect
        /// </summary>
        public event Action<int> ConfirmConn;
        public event Action<String, String> SetClientCell;
        public event Action<int, String> Error;

        public Communicator(string hostname, int port)
        {
            if (serverConn == null)
            {
                client = new TcpClient(hostname, port);
                serverConn = new StringSocket(client.Client, UTF8Encoding.Default);
            }
        }

        /// <summary>
        /// Deals with all messages from the Server to the Client
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="e"></param>
        /// <param name="payload"></param>
        private void MessageReceived(string msg, Exception e, object payload)
        {
            System.Diagnostics.Debug.WriteLine("msg is " + msg);
            // If the server sends junk, don't crash
            if(msg.Length < 4)
                Error(0, "The message from the server is less than 4 characters");

            String msgStart = msg.Substring(0, 4);

            switch (msgStart)
            {
                case "conn":
                    String numCells = msg.Substring(10);

                    int cellCount;
                    Int32.TryParse(numCells, out cellCount);
                    ConfirmConn(cellCount);
                    break;
                case "cell":
                    String msgEnd = msg.Substring(5);
                    String cellName = msgEnd.Substring(0, msgEnd.IndexOf(" "));
                    String cellContents = msgEnd.Substring(msgEnd.IndexOf(" ")).Substring(1); // remove space
                    SetClientCell(cellName, cellContents);
                    break;
                case "erro":
                    int errorID;
                    Int32.TryParse(msg.Substring(6, 1), out errorID);
                    String errorMsg = msg.Substring(8);
                    Error(errorID, errorMsg);
                    break;
                default:
                    throw new InvalidOperationException(msg);
            }
            serverConn.BeginReceive(MessageReceived, null);
        }


        //==================== DEALS WITH ALL MESSAGES FROM CLIENT TO SERVER ====================//

        public void Connect(String userName, String spreadsheetName)
        {
            serverConn.BeginSend("connect " + userName + " " + spreadsheetName + "\n", (e, p) => { }, null);
            serverConn.BeginReceive(MessageReceived, null);
        }

        public void Register(String userName) 
        {
            String msg = "register " + userName + "\n";
            serverConn.BeginSend(msg, (e, p) => { }, null);
            System.Diagnostics.Debug.WriteLine(msg);
        }

        public void SetServerCell(String cell_name, String cell_contents)
        {
            // TODO: make sure cell_name and cell_contents don't have \n's\
            // It is okay if cell_contents has spaces, b/c everything after 
            // cell_name is contents until a newline
            String msg = "cell " + cell_name + " " + cell_contents + "\n";
            serverConn.BeginSend(msg, (e, p) => { }, null);
            System.Diagnostics.Debug.WriteLine(msg);
        }

        public void Undo()
        {
            String msg = "undo\n";
            serverConn.BeginSend(msg, (e, p) => { }, null);
            System.Diagnostics.Debug.WriteLine(msg);
        }

    }
}
