﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using SpreadsheetUtilities;

namespace SS
{
    /// <summary>
    /// A Unit tst class for the Spreadsheet and its components
    /// </summary>
    /// <modifier>Fahad A Alothaimeen</modifier>
    /// <uNID> U0824563 </uNID>
    [TestClass]
    public class SSUnitTest
    {
        /// <summary>
        /// Tests the contructor behavior of the Spreadsheet
        /// </summary>
        [TestMethod]
        public void ConstructorTest1()
        {
            AbstractSpreadsheet sheet = new Spreadsheet();

            // making sure all cells are empty at construction time
            HashSet<string> CellsSet = (HashSet<string>)sheet.GetNamesOfAllNonemptyCells();
            Assert.AreEqual(0, CellsSet.Count);
        }

        /// <summary>
        /// Tests the contructor behavior of the Spreadsheet,
        /// given a filePath of an empty XML file
        /// </summary>
        [TestMethod]
        public void ConstructorTest2()
        {
            AbstractSpreadsheet sheet = new Spreadsheet();
            if (sheet.Changed) { sheet.Save("test2"); }
            AbstractSpreadsheet sheet2 = new Spreadsheet(@".\test2.xml", s => true, s => s, "default");

            // making sure all cells are empty at construction time
            HashSet<string> CellsSet = (HashSet<string>)sheet.GetNamesOfAllNonemptyCells();
            Assert.AreEqual(0, CellsSet.Count);
        }

        /// <summary>
        /// Tests the contructor behavior of the Spreadsheet,
        /// given a filePath of a non-empty XML file
        /// </summary>
        [TestMethod]
        public void ConstructorTest3()
        {
            AbstractSpreadsheet sheet = new Spreadsheet(s => true, s => s.ToUpper(), "1");

            sheet.SetContentsOfCell("a1", "5");
            sheet.SetContentsOfCell("b1", "7");
            sheet.SetContentsOfCell("c1", "=a1+b1");
            sheet.SetContentsOfCell("d1", "=a1*c1");
            sheet.SetContentsOfCell("e1", "=C1+D1");

            if (sheet.Changed) { sheet.Save("test3"); }
            AbstractSpreadsheet sheet2 = new Spreadsheet(@".\test3.xml", s => true, s => s, "1");

            // making sure there are 5 cells
            HashSet<string> CellsSet = (HashSet<string>)sheet.GetNamesOfAllNonemptyCells();
            Assert.AreEqual(5, CellsSet.Count);
        }

        /// <summary>
        /// Tests the contructor behavior of the Spreadsheet,
        /// When the version given is different from the version requested
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(SpreadsheetReadWriteException))]
        public void ConstructorTest4()
        {
            AbstractSpreadsheet sheet = new Spreadsheet(s => true, s => s.ToUpper(), "1");

            sheet.SetContentsOfCell("a1", "5");
            sheet.SetContentsOfCell("b1", "7");
            sheet.SetContentsOfCell("c1", "=a1+b1");
            sheet.SetContentsOfCell("d1", "=a1*c1");
            sheet.SetContentsOfCell("e1", "=C1+D1");

            if (sheet.Changed) { sheet.Save("test4"); }
            AbstractSpreadsheet sheet2 = new Spreadsheet(@".\test4.xml", s => true, s => s, "2");
        }

        /// <summary>
        /// Tests the bevavior of "GetSavedVersion" function
        /// </summary>
        [TestMethod]
        public void SavedVersionTest1()
        {
            AbstractSpreadsheet sheet = new Spreadsheet(s => true, s => s, "1");
            if (sheet.Changed) { sheet.Save("test5"); }

            Assert.AreEqual("1", sheet.GetSavedVersion("test5"));
        }

        /// <summary>
        /// Tests the bevavior of "GetSavedVersion" function,
        /// given a ".xml" extension
        /// </summary>
        [TestMethod]
        public void SavedVersionTest2()
        {
            AbstractSpreadsheet sheet = new Spreadsheet(s => true, s => s, "1");
            if (sheet.Changed) { sheet.Save("test6"); }

            Assert.AreEqual("1", sheet.GetSavedVersion("test6.xml"));
        }

        /// <summary>
        /// Tests the bevavior of "GetSavedVersion" function,
        /// when XML file does not exist
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(SpreadsheetReadWriteException))]
        public void SavedVersionTest3()
        {
            AbstractSpreadsheet sheet = new Spreadsheet();

            Assert.AreEqual("1", sheet.GetSavedVersion("testNull.xml"));
        }

        /// <summary>
        /// Tests the abstract behavior of having infinitly 
        /// many cells
        /// </summary>
        [TestMethod]
        public void infintCellsTest()
        {
            AbstractSpreadsheet sheet = new Spreadsheet();

            // Making sure we have an infintily empty cells
            Assert.AreEqual("", sheet.GetCellContents("A1"));
        }

        /// <summary>
        /// Tests the finctionality of getting 
        /// any given cell value
        /// </summary>
        [TestMethod]
        public void ValueCheckTest1()
        {
            AbstractSpreadsheet sheet = new Spreadsheet();

            sheet.SetContentsOfCell("A1", "");

            // Making sure we have the right value
            Assert.AreEqual("", sheet.GetCellValue("A1"));

            // making sure all cells are still empty
            HashSet<string> CellsSet = (HashSet<string>)sheet.GetNamesOfAllNonemptyCells();
            Assert.AreEqual(0, CellsSet.Count);
        }

        /// <summary>
        /// Tests the finctionality of getting 
        /// any given cell value
        /// </summary>
        [TestMethod]
        public void ValueCheckTest2()
        {
            AbstractSpreadsheet sheet = new Spreadsheet();

            sheet.SetContentsOfCell("A1", "5");

            // Making sure we have the right value
            Assert.AreEqual(5.0, sheet.GetCellValue("A1"));

            // Making sure at least one cell is occupied
            HashSet<string> CellsSet = (HashSet<string>)sheet.GetNamesOfAllNonemptyCells();
            Assert.AreEqual(1, CellsSet.Count);
        }

        /// <summary>
        /// Tests the finctionality of getting 
        /// any given cell value
        /// </summary>
        [TestMethod]
        public void ValueCheckTest3()
        {
            AbstractSpreadsheet sheet = new Spreadsheet(s => true, s => s.ToUpper(), "1");

            sheet.SetContentsOfCell("a1", "5");
            sheet.SetContentsOfCell("b1", "7");
            sheet.SetContentsOfCell("c1", "=a1+b1");
            sheet.SetContentsOfCell("d1", "=a1*c1");
            sheet.SetContentsOfCell("e1", "=C1+D1");

            // Making sure we have the right value
            Assert.AreEqual(72.0, sheet.GetCellValue("E1"));

            // Making sure at least 5 cell is occupied
            HashSet<string> CellsSet = (HashSet<string>)sheet.GetNamesOfAllNonemptyCells();
            Assert.AreEqual(5, CellsSet.Count);
        }

        /// <summary>
        /// Tests the finctionality of getting 
        /// any given cell value
        /// </summary>
        [TestMethod]
        public void ValueCheckTest4()
        {
            AbstractSpreadsheet sheet = new Spreadsheet();

            // Making sure we have the right value
            Assert.AreEqual("", sheet.GetCellValue("A1"));

            // making sure all cells are still empty
            HashSet<string> CellsSet = (HashSet<string>)sheet.GetNamesOfAllNonemptyCells();
            Assert.AreEqual(0, CellsSet.Count);
        }

        /// <summary>
        /// Tests the finctionality of getting 
        /// any given cell value when the cell name is not valid
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(InvalidNameException))]
        public void ValueCheckTest5()
        {
            AbstractSpreadsheet sheet = new Spreadsheet();

            // Making sure we have the right value
            Assert.AreEqual("", sheet.GetCellValue("1A"));
        }

        /// <summary>
        /// Tests the behavior when adding an empty cell 
        /// to the Spreadsheet
        /// </summary>
        [TestMethod]
        public void ContentsCheckTest1()
        {
            AbstractSpreadsheet sheet = new Spreadsheet();

            sheet.SetContentsOfCell("A1", "");

            // Making sure we have the right content
            Assert.AreEqual("", sheet.GetCellContents("A1"));

            // making sure all cells are still empty
            HashSet<string> CellsSet = (HashSet<string>)sheet.GetNamesOfAllNonemptyCells();
            Assert.AreEqual(0, CellsSet.Count);
        }

        /// <summary>
        /// Tests the behavior when adding a string cell 
        /// to the Spreadsheet
        /// </summary>
        [TestMethod]
        public void ContentsCheckTest2()
        {
            AbstractSpreadsheet sheet = new Spreadsheet();

            sheet.SetContentsOfCell("A1", "Hello");

            // Making sure we have the right content
            Assert.AreEqual("Hello", sheet.GetCellContents("A1"));

            // Making sure at least one cell is occupied
            HashSet<string> CellsSet = (HashSet<string>)sheet.GetNamesOfAllNonemptyCells();
            Assert.AreEqual(1, CellsSet.Count);
        }

        /// <summary>
        /// Tests the behavior when adding a "double" cell 
        /// to the Spreadsheet
        /// </summary>
        [TestMethod]
        public void ContentsCheckTest3()
        {
            AbstractSpreadsheet sheet = new Spreadsheet();

            sheet.SetContentsOfCell("A1", "5");

            // Making sure we have the right content
            Assert.AreEqual(5.0, sheet.GetCellContents("A1"));

            // Making sure at least one cell is occupied
            HashSet<string> CellsSet = (HashSet<string>)sheet.GetNamesOfAllNonemptyCells();
            Assert.AreEqual(1, CellsSet.Count);
        }

        /// <summary>
        /// Tests the behavior when adding a formula cell 
        /// to the Spreadsheet
        /// </summary>
        [TestMethod]
        public void ContentsCheckTest4()
        {
            AbstractSpreadsheet sheet = new Spreadsheet();

            sheet.SetContentsOfCell("A1", "=5+5");

            // Making sure we have the right content
            Assert.AreEqual(new Formula("=5+5"), sheet.GetCellContents("A1"));

            // Making sure at least one cell is occupied
            HashSet<string> CellsSet = (HashSet<string>)sheet.GetNamesOfAllNonemptyCells();
            Assert.AreEqual(1, CellsSet.Count);
        }

        /// <summary>
        /// Tests the behavior when changing the content 
        /// of a cell from empty to string
        /// </summary>
        [TestMethod]
        public void ContentReAssignedTest1()
        {
            AbstractSpreadsheet sheet = new Spreadsheet();

            sheet.SetContentsOfCell("A1", "");
            sheet.SetContentsOfCell("A1", "Hello");

            // Making sure we have the right content
            Assert.AreEqual("Hello", sheet.GetCellContents("A1"));

            // Making sure at least one cell is occupied
            HashSet<string> CellsSet = (HashSet<string>)sheet.GetNamesOfAllNonemptyCells();
            Assert.AreEqual(1, CellsSet.Count);
        }

        /// <summary>
        /// Tests the behavior when changing the content 
        /// of a cell from string to double
        /// </summary>
        [TestMethod]
        public void ContentReAssignedTest2()
        {
            AbstractSpreadsheet sheet = new Spreadsheet();

            sheet.SetContentsOfCell("A1", "Hello");
            sheet.SetContentsOfCell("A1", "5");

            // Making sure we have the right content
            Assert.AreEqual(5.0, sheet.GetCellContents("A1"));

            // Making sure at least one cell is occupied
            HashSet<string> CellsSet = (HashSet<string>)sheet.GetNamesOfAllNonemptyCells();
            Assert.AreEqual(1, CellsSet.Count);
        }

        /// <summary>
        /// Tests the behavior when changing the content 
        /// of a cell from double to formula
        /// </summary>
        [TestMethod]
        public void ContentReAssignedTest3()
        {
            AbstractSpreadsheet sheet = new Spreadsheet(s => true, s => s.ToUpper(), "default");

            sheet.SetContentsOfCell("A1", "5");
            sheet.SetContentsOfCell("A1", "=c1+b2");

            // Making sure we have the right content
            Assert.AreEqual(new Formula("=C1+B2"), sheet.GetCellContents("A1"));

            // Making sure at least one cell is occupied
            HashSet<string> CellsSet = (HashSet<string>)sheet.GetNamesOfAllNonemptyCells();
            Assert.AreEqual(1, CellsSet.Count);
        }

        /// <summary>
        /// Tests the behavior when getting content 
        /// of a cell using inValid cell name
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(InvalidNameException))]
        public void ContentsExceptionTest1()
        {
            AbstractSpreadsheet sheet = new Spreadsheet();

            // Making sure the right exception is thrwon after this call
            sheet.GetCellContents("&1");
        }

        /// <summary>
        /// Tests the behavior when getting content 
        /// of a cell using null cell
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(InvalidNameException))]
        public void ContentsExceptionTest2()
        {
            AbstractSpreadsheet sheet = new Spreadsheet();

            // Making sure the right exception is thrwon after this call
            sheet.GetCellContents(null);
        }

        /// <summary>
        /// Tests the behavior when setting content 
        /// of cell using inValid cell name
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(InvalidNameException))]
        public void ContentsExceptionTest3a()
        {
            AbstractSpreadsheet sheet = new Spreadsheet();

            // Making sure the right exception is thrwon after this call
            sheet.SetContentsOfCell("&1", "Hello");
        }

        /// <summary>
        /// Tests the behavior when setting content 
        /// of cell using null cell
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(InvalidNameException))]
        public void ContentsExceptionTest3b()
        {
            AbstractSpreadsheet sheet = new Spreadsheet();

            // Making sure the right exception is thrwon after this call
            sheet.SetContentsOfCell(null, "Hello");
        }

        /// <summary>
        /// Tests the behavior when setting content 
        /// of cell using null content
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException),
         "inValid null text")]
        public void ContentsExceptionTest3c()
        {
            AbstractSpreadsheet sheet = new Spreadsheet();

            string str = null;

            // Making sure the right exception is thrwon after this call
            sheet.SetContentsOfCell("A1", str);
        }

        /// <summary>
        /// Tests the behavior of the Spreadsheet when 
        /// Circular Exception occurs
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(CircularException))]
        public void ContentsExceptionTest4()
        {
            AbstractSpreadsheet sheet = new Spreadsheet();

            sheet.SetContentsOfCell("A1", "=B1*2");
            sheet.SetContentsOfCell("B1", "=C1*2");

            // Making sure the right exception is thrwon after this call
            sheet.SetContentsOfCell("C1", "=A1*2");
        }

        /// <summary>
        /// Tests the behavior when setting content 
        /// of a string cell using inValid cell name
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(InvalidNameException))]
        public void ContentsExceptionTest5()
        {
            AbstractSpreadsheet sheet = new Spreadsheet();
            PrivateObject objTester = new PrivateObject(sheet);

            // Making sure the right exception is thrwon after this call
            objTester.Invoke("SetCellContents", new object[] { "&1", "Hello" });
        }

        /// <summary>
        /// Tests the behavior when setting content 
        /// of a "double" cell using inValid cell name
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(InvalidNameException))]
        public void ContentsExceptionTest7()
        {
            AbstractSpreadsheet sheet = new Spreadsheet();
            PrivateObject objTester = new PrivateObject(sheet);

            // Making sure the right exception is thrwon after this call
            objTester.Invoke("SetCellContents", new object[] { "&1", 5.5 });
        }

        /// <summary>
        /// Tests the behavior when setting content 
        /// of a Formula cell using inValid cell name
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(InvalidNameException))]
        public void ContentsExceptionTest8()
        {
            AbstractSpreadsheet sheet = new Spreadsheet();
            PrivateObject objTester = new PrivateObject(sheet);

            // Making sure the right exception is thrwon after this call
            objTester.Invoke("SetCellContents", new object[] { "&1", new Formula("=5+5") });
        }

        /// <summary>
        /// Tests the behavior of the Cell class GetValue() method
        /// </summary>
        [TestMethod]
        public void CellClassTest()
        {
            Dictionary<string, Cell> CellsSoFar = new Dictionary<string, Cell>();

            Cell c1 = new Cell(5);
            Cell c2 = new Cell(5);
            Cell c3 = new Cell(5);

            CellsSoFar.Add("A1", c1);
            CellsSoFar.Add("B1", c2);
            CellsSoFar.Add("C1", c3);

            Cell c4 = new Cell(new Formula("A1*B1*C1"), CellsSoFar);

            // Making sure we have the right value
            Assert.AreEqual(125.0, c4.getCellValue());
        }

        /// <summary>
        /// Tests the behavior of GetDirectDependents() 
        /// method when given null prameter
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException),
         "inValid null name")]
        public void DirectDependencyExceptionTest1()
        {
            AbstractSpreadsheet sheet = new Spreadsheet();
            PrivateObject objTester = new PrivateObject(sheet);

            // Making sure the right exception is thrwon after this call
            objTester.Invoke("GetDirectDependents", new object[] { null });
        }

        /// <summary>
        /// Tests the behavior of GetDirectDependents() 
        /// method when given inValid cell name
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(InvalidNameException))]
        public void DirectDependencyExceptionTest2()
        {
            AbstractSpreadsheet sheet = new Spreadsheet();
            PrivateObject objTester = new PrivateObject(sheet);

            // Making sure the right exception is thrwon after this call
            objTester.Invoke("GetDirectDependents", new object[] { "&1" });
        }
    }
}
