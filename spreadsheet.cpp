#include "spreadsheet.h"

using namespace boost::algorithm;
using namespace std;

/** Creates a new spreadsheet with the given filename.
 * If the file does not exist it is created.
 * If the file does exist the spreadsheet data is taken from the file.
 */
spreadsheet::spreadsheet(string dbFilename, bool consoleLog){
  this->consoleLog = consoleLog;
  sqlite3_open(dbFilename.c_str(), &db);
  string sql = "Create table cells(name text, contents text)";
  sqlite3_stmt* prepStmt;
  sqlite3_prepare_v2(db, sql.c_str(), -1, &prepStmt, NULL);
  sqlite3_step(prepStmt);
  cout << "Starting to finalize" << endl;
  sqlite3_finalize(prepStmt);
  cout << "Finalized" << endl;
  
  this->name = dbFilename;
}

/** Determines if two spreadsheets are equal 
 * by determining if they have the same name
 */
bool spreadsheet::operator==(spreadsheet &rhs){
  if(this->name == rhs.name)
    return true;
  return false;
}

/**
 * Returns the name of the spreadsheet
 */
std::string spreadsheet::getName(){
	return this->name;
}

/** Deletes all the cells from this spreadsheet
 */
void spreadsheet::clear(){
  const char* sql = "Delete from cells;";
  sqlite3_stmt* prepStmt;
  sqlite3_prepare_v2(db, sql, -1, &prepStmt, NULL);
  if(sqlite3_step(prepStmt) != SQLITE_DONE)
		cout << "DB Error: " << sqlite3_errmsg(db) << endl;
	sqlite3_finalize(prepStmt);
}
    

/** Returns the contents of the cell with the given name
 */
string spreadsheet::cell(string name){
  char*  sql = sqlite3_mprintf("Select contents from cells where name = '%q';", name.c_str());
  log(sql);

  // Prepare the statement
  sqlite3_stmt* prepStmt;

  int prepStatus = sqlite3_prepare_v2(db, sql, -1, &prepStmt, NULL);

  // SQLITE_ROW means it returned a row, SQLITE_DONE means there are no more rows
  if(prepStatus != SQLITE_OK){
    cerr << "Db prep error: " << prepStatus << " " << sqlite3_errmsg(db) << endl;
    return sqlite3_errmsg(db);

  }

  string contents = "";
  int stepStatus = sqlite3_step(prepStmt);

  // Check for an error in the step
  if(stepStatus != SQLITE_DONE && stepStatus != SQLITE_ROW){
    cerr << "DB Step Err " << stepStatus << " : " << sqlite3_errmsg(db) << endl;
    return sqlite3_errmsg(db);
  }

  // Get the contents of the row
  if(stepStatus == SQLITE_ROW){
    contents = (char*)sqlite3_column_text(prepStmt, 0);
  }
	sqlite3_finalize(prepStmt);
  return contents;
}

int spreadsheet::cellCount(){
  string sql = "Select Count(*) from cells";
  log(sql);

  // Prepare the statement
  sqlite3_stmt* prepStmt;
  int prepStatus = sqlite3_prepare_v2(db, sql.c_str(), -1, &prepStmt, NULL);

  // SQLITE_ROW means it returned a row, SQLITE_DONE means there are no more rows
  if(prepStatus != SQLITE_OK){
    cerr << "Db prep error: " << prepStatus << " " << sqlite3_errmsg(db) << endl;
    throw -2;
  }

  // Get the contents of the row
  int cellCount = -1;
  int stepStatus = sqlite3_step(prepStmt);
  if(stepStatus == SQLITE_ROW){
    cellCount = sqlite3_column_int(prepStmt, 0);
  }
	sqlite3_finalize(prepStmt);
  return cellCount;
}

bool spreadsheet::containsCell(string cellName){
  const char* sql = "Select Count(*) from cells Where name = '%q'";
  const char* sqlEsc = sqlite3_mprintf(sql, cellName.c_str());
  log(sqlEsc);

  // Prepare the statement
  sqlite3_stmt* prepStmt;
  int prepStatus = sqlite3_prepare_v2(db, sqlEsc, -1, &prepStmt, NULL);

  // SQLITE_ROW means it returned a row, SQLITE_DONE means there are no more rows
  if(prepStatus != SQLITE_OK){
    cerr << "Db prep error: " << prepStatus << " " << sqlite3_errmsg(db) << endl;
    throw -2;
  }

  // Get the number of cells with the name
  int cellCount = -1;
  int stepStatus = sqlite3_step(prepStmt);
  if(stepStatus == SQLITE_ROW){
    cellCount = sqlite3_column_int(prepStmt, 0);
  }
	sqlite3_finalize(prepStmt);
  return (cellCount > 0);
}
  
/** Sets the contents of a cell.
 * @param name the name of the cell to modify.
 * @param contents the contents of the cell being modified.
 */
bool spreadsheet::setCell(string name, string contents){
  // If contents is a formula, validate formula contents and 
  // store any cell dependencies
  if( contents[0] == '=' ){
    // Get all cells who depend on the current cell
    std::set<std::string> dependents = parseCellContents(contents);
	
	// TESTING: DELETEME
	/*cout << "Dependents: " << endl;
	for(std::set<std::string>::iterator it = dependents.begin(); it != dependents.end(); it++){
		cout << "\t" << *it << endl;
	}*/

    // Check for circular dependencies
    if(findCycle(name, dependents)){
		// TESTING: DELETEME
		//cout << "LOOK: setCell: find cycle returned true" << endl;
  		return false;
	}  
    // Add dependencies to dependentsMap
    addDependents(name, dependents);
  }

  // Save previous value of cell in undo list
  std::string prevContents = cell(name);
  prevCellValues.push(name + " " + prevContents);

  // If the contents are blank then remove the cell
  trim(contents);
  if(contents == "")
    removeCell(name);
  // Else check if the cell exists in the db
  // If it does, update it, if not insert it
  else{
    if(containsCell(name))
          updateCell(name, contents);
    // If the number is 0, the cell needs to be inserted
    else
        insertCell(name, contents);
  }
  return true;
}

/** Returns a vector containing all the cells contained in this spreadsheet.
 */
vector<spreadsheet::cellInfo> spreadsheet::cells(){
  vector<cellInfo> cells;
  string sql = "Select * from cells";
  log(sql);

  // Prepare the statment
  sqlite3_stmt* prepStmt;
  if(sqlite3_prepare_v2(db, sql.c_str(), -1, &prepStmt, NULL) != SQLITE_OK)
    cerr << "Db Error: " << sqlite3_errmsg(db) << endl;

  if(prepStmt){
    // Add a cell to the cells vector for every cell in the table
    int stepStatus = sqlite3_step(prepStmt);
    for(; stepStatus == SQLITE_ROW; stepStatus = sqlite3_step(prepStmt)){
      cellInfo cell;
      cell.name = (char*)sqlite3_column_text(prepStmt, 0);
      cell.contents = (char*)sqlite3_column_text(prepStmt, 1);
      string nameMsg = "cell name: ";
      nameMsg += cell.name;
      log(nameMsg);
      string contentsMsg = "cell contents: ";
      contentsMsg += cell.contents;
      log(contentsMsg);
      cells.push_back(cell);
    }

    if(stepStatus != SQLITE_DONE)
      cerr << "DB Err: " << sqlite3_errmsg(db) << endl;
  }
  else
    cerr << "Err: prepStmt is NULL" << endl;

	sqlite3_finalize(prepStmt);
  return cells;
}

/**
* Undoes the last change made to the current spreadsheet and returns the command
* used to revert back to the old contents
*/
std::string spreadsheet::undo(){
//return "I am in the spreadsheet's undo method";
  if(!prevCellValues.empty()){
	  std::string oldNameAndContents = prevCellValues.top();
	  prevCellValues.pop();

	  std::size_t positionOfSpace = oldNameAndContents.find(" ");
	  std::string oldName = oldNameAndContents.substr(0, positionOfSpace);
	  std::string oldContents = oldNameAndContents.substr(positionOfSpace + 1);

	  setCell(oldName, oldContents);

	  return oldNameAndContents;
  }
  return "";
}

/** Closes this spreadsheet
 */
void spreadsheet::close(){
  sqlite3_close(db);
}


/** Sets the contents of cell in the database to the given contents
 */
void spreadsheet::updateCell(string name, string contents){
  const char* sql = sqlite3_mprintf("Update cells set contents = '%q' WHERE name = '%q';", 
      contents.c_str(), name.c_str());
  log(sql);

  char* errMsg;
  int err;
  // Add a cell to the db
  err = sqlite3_exec(db, sql,  0, 0, &errMsg);
  if(err != SQLITE_OK){
    cout << "SQL execution error " << errMsg << endl;
  }            
}
    
/** Adds a cell to the database with the given name and contents
 */
void spreadsheet::insertCell(string name, string contents){
  const char* sql = sqlite3_mprintf("Insert into cells(name, contents) Values('%q', '%q');", 
                              name.c_str(), contents.c_str());
  log(sql);
      
  char* errMsg;
  int err;
  // Set cell to the new contents
  err = sqlite3_exec(db, sql,  0, 0, &errMsg);
  if(err != SQLITE_OK){
    cout << "SQL execution error " << errMsg << endl;
  }
}

/** Removes the cell from the database with the given name
  */
void spreadsheet::removeCell(string name){
  char* sql = sqlite3_mprintf("DELETE FROM cells WHERE name = '%q';", name.c_str());
  log(sql);
      
  char* errMsg;
  int err;
  // Set cell to the new contents
  err = sqlite3_exec(db, sql, 0, 0, &errMsg);
  if(err != SQLITE_OK){
    cout << "SQL execution error " << errMsg << endl;
  }

}


// ----- The following helper methods are used to keep track of cell dependencies-------//
// -------------and to check for circular references in cell contents-------------------//

/**
 * Stores all cellNames in the given contents into a set and returns the set
 */
std::set<std::string> spreadsheet::parseCellContents (std::string cellContents) {
  
  std::vector<std::string> dependents;
  boost::split(dependents, cellContents, boost::is_any_of("()+-=*/ "));  // Separate all cellNames

  std::set<std::string> dependentsSet;

  // Verify cellName is valid. If so, add it to dependentsSet
  for(std::vector<std::string>::iterator it = dependents.begin(); it != dependents.end(); ++it){

		bool valid = true;
		bool isInt = false;
		bool isAlpha = false;
		
		std::string cellName = *it;
		for (uint8_t i = 0; i < cellName.length(); i++)
		{
		    if (!isInt)
		    {
				if(isalpha(cellName[i])){
					isAlpha = true;
					continue;
				}
				else if(isdigit(cellName[i]))
					isInt = true;
				else{
			        valid = false;
					break;
				}
		    }
			else if((isInt && !isdigit(cellName[i]))  ){ 
				valid = false;
				break;
			}
		}
		if(valid && isInt && isAlpha){
			dependentsSet.insert(cellName);
		}
  }

  return dependentsSet;
}


/**
 * Determines if a cell has circular dependencies
 */
bool spreadsheet::findCycle(std::string cellName, std::set<std::string> dependents){

  // Maps a cellName to a bool (true for visited, false otherwise)
  // to keep track of whether a node in a dependency graph has been 
  // checked for dependents at all
  std::map<std::string, bool> visited;

  // Maps a cellName to a bool (true for visited, false otherwise)
  // to keep track of whether a node in a dependency graph has been 
  // checked for dependents in a specific branch of the map
  std::map<std::string, bool> onStack;

  // Denotes whether or not a cell has a circular dependency
  bool hasCycle = false;

  findCycleRecursive(cellName, dependents, visited, onStack, hasCycle);
  return hasCycle;
}	

/** 
 * Recursive helper method for findCycle to determine if a cell has circular dependencies
 *
 * Citation: Idea and structure reference by Stack Overflow at 
 * :http://stackoverflow.com/questions/19113189/detecting-cycles-in-a-graph-using-dfs-2-different-approaches-and-whats-the-dif
 */
void spreadsheet::findCycleRecursive(std::string cellName, std::set<std::string> dependents, 
      std::map<std::string, bool> &visited, std::map<std::string, bool> &onStack,
      bool &hasCycle){


  visited[cellName] = true;
  onStack[cellName] = true;

  // Iterate over all dependents and check their dependents recursively
  for (std::set<std::string>::iterator it = dependents.begin(); it != dependents.end(); ++it){

    if( !visited[*it] )
		findCycleRecursive(*it, dependentsMap[*it], visited, onStack, hasCycle);
    else if ( onStack[*it] ){
		hasCycle = true;
		return;
    }
  } 
  // Remove this cellName from the current map's branch you are searching through
  onStack[cellName] = false;
}

/**
 * Adds the dependencies for the given cellName and all cellNames in the set dependents
 */
void spreadsheet::addDependents (std::string cellName, std::set<std::string> dependents) {

  for (std::set<std::string>::iterator it = dependents.begin(); it != dependents.end(); ++it){
    dependentsMap[cellName].insert(*it);
  }

}

void spreadsheet::log(string msg){
  if(consoleLog)
    cout << msg << endl;
}

string appendNum(string original, int num){
  return original + boost::lexical_cast<string>(num);
}


