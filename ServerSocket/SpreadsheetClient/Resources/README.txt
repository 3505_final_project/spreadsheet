﻿Name: Fahad A Alothaimeen

Date: 11/02/2014


################################ Thoughts of the Spreadsheet GUI Project #################################

>>> This file contains spreadsheet GUI implementation process


> Start of Day 1: 11/02/2014 @ 1pm

> Just started the project.

> Branching is completed successfully.

> Struggling with how to start.

> Struggling with how to use the Spreadsheet Panel and setup my GUI.

> The spreadsheet Panel is used properly and embedded into the GUI.

> The new Application functionality is implemented properly.

> First day of programming is finished!


> Start of Day 2: 11/03/2014 @ 7am

> Struggling with how to Hanle events on the Panel.

> Events are handled correctly.

> Cell Dependencies are implemented successfully.

> Struggling with adding menu strip items.

> Menu strip items are added successfully but not implemented yet.

> Struggling so much on how to prevent scroll bars from capturing keystrokes
events.

> Second day of programming is finished!


> Start of Day 3: 11/04/2014 @ 9am

> Struggling with making the keystrokes moves between cells without
Introducing new bugs to the program.

> Still struggling and searching for help.

> Text Boxes are implemented properly.

> The ReSet button is implemented successfully.

> Finally the Keystrokes are implemented properly without errors by using two
different modes in the application.

> Started to add more features: Thinking of a way to show the help messages.

> Safety features are implemented correctly.

> Third day of programming is finished!


> Start of Day 4: 11/05/2014 @ 8am

> Figured out a way to show the help and the feature messages.

> Help menu item is implemented correctly.

> Colors were changes.

> UIUnit Tests are at their final stages.

> Added 4 more UIUnit Tests.

> Fixed some bugs and misplaces code blocks.

> Rearrange code for better coding style.

> Private helper methods are implemented successfully to reduce the number of code lines.

> Comments were added successfully.

> The project is considered finished (11/05/2014 @ 10:25pm)


################################ End of thoughts #################################