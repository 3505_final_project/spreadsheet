************************ ADDED FEATURES ************************

-The following list has the features that was added to this
Spreadsheet by the developer:

1- Keystrokes Cell Selection: New way to choose between different Cells
When in Spreadsheet mode.

2- "ReSet" Button: this button will allow clearing all current contents
the spreadsheet without the need to open new window or an empty file
or clearing the cells one by one and waste time.

3- Save/SaveAs Feature: this feature will allow you either to save the Spreadsheet
to a specific path, or to the current path of the document (if found).

4- Program Safety Modes: the program is governed by two separate modes that
make it safer and less "buggy".

5- Using the "Tab" key to toggle quickly between modes.

6- New icon and Pretty colors for the application.