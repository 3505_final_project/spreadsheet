//
// blocking_tcp_echo_client.cpp
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//
// Copyright (c) 2003-2012 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

#include <cstdlib>
#include <cstring>
#include <iostream>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/thread/thread.hpp>


using boost::asio::ip::tcp;

class client
{
public:
  client(boost::asio::io_service& io_service,
      const std::string& server, const std::string& port)
    : io_service_(io_service), resolver_(io_service),
      query_(tcp::v4(), server, port),
      socket_(io_service)
  {
    iterator = resolver_.resolve(query_);

    boost::asio::async_connect(socket_, iterator,
      boost::bind(&client::connection_recieved, this,
          boost::asio::placeholders::error));
  }

  void send(std::string msg)
  {
    io_service_.post(boost::bind(&client::begin_send, this, msg));
  }

  void close()
  {
    io_service_.post(boost::bind(&client::do_close, this));
  }

private:

  void connection_recieved(const boost::system::error_code& error)
  {
    if (!error)
    {
      socket_.async_read_some(boost::asio::buffer(reply, max_length),
          boost::bind(&client::message_received, this,
            boost::asio::placeholders::error,
            boost::asio::placeholders::bytes_transferred));
    }
  }

  void message_received(const boost::system::error_code& error,
      size_t bytes_transferred)
  {
    if (!error)
    {
      std::cout << "\n";
      std::cout << "Reply is: ";
      std::cout.write(reply, bytes_transferred);
      std::cout << "\n\n";

      socket_.async_read_some(boost::asio::buffer(reply, max_length),
        boost::bind(&client::message_received, this,
          boost::asio::placeholders::error,
          boost::asio::placeholders::bytes_transferred));
    }
    else
    {
      std::cout << "Error: " << error.message() << "\n";
      do_close();
    }
  }

  void begin_send(std::string msg)
  {
    boost::asio::async_write(socket_,
      boost::asio::buffer(msg.c_str(), msg.size()),
      boost::bind(&client::message_sent, this,
      boost::asio::placeholders::error));
    
  }

  void message_sent(const boost::system::error_code& error)
  {
    if (!error)
    {
    }
    else
    {
      std::cout << "Error: " << error.message() << "\n";
      do_close();
    }
  }

  void do_close()
  {
    socket_.close();
  }

  boost::asio::io_service& io_service_;
  tcp::resolver resolver_;
  tcp::resolver::query query_;
  tcp::socket socket_;
  tcp::resolver::iterator iterator;

  enum { max_length = 1024 };
  char request[max_length];
  char reply[max_length];
  size_t request_length;
};




int main(int argc, char* argv[])
{
  try
  {
    if (argc != 3)
    {
      std::cerr << "Usage: blocking_tcp_echo_client <host> <port>\n";
      return 1;
    }
    boost::asio::io_service io_service;
    client c(io_service, argv[1], argv[2]);

    boost::thread t(boost::bind(&boost::asio::io_service::run, &io_service));

    enum { max_length = 1024 };
    char line[max_length];
    while (std::cin.getline(line, max_length))
    {
      std::string msg(line, strlen(line));
      c.send(msg);
    }

    c.close();
    t.join();    
  }
  catch (std::exception& e)
  {
    std::cerr << "Exception: " << e.what() << "\n";
  }

  return 0;
}
