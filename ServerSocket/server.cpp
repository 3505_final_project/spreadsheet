/**
 * Cs3505 Final Project
 * Group Name: 100 Bucks
 * Group Members: Ryan Kingston, Joshua Oblinsky, Fahad Tmem, Taylor Jenkins
 *
 * A simple implementation of a tcp connection between a client and a server.
 * The server will echo back information sent to it from the client.
 */

#include "../spreadsheet.h"
#include <cstdlib>
#include <iostream>
#include <boost/bind.hpp>
#include <boost/asio.hpp>
#include <boost/lexical_cast.hpp>
#include <queue>
#include <boost/thread/thread.hpp>
#include <fstream>
#include <list>
#include<unistd.h>
//#include <thread>
//#include <chrono> 
//#include <cstdlib>


using boost::asio::ip::tcp;


/**
 * Class to save recieved messages
 *
 **/
class messages_pocket
{
  friend class client_representative;
public:
  messages_pocket()
  {
    index = 0;
    incoming_message = "";
  }

  void push_str(char* _data, size_t bytes_transferred)
  {
    for(size_t i = 0; i < bytes_transferred; i++)
      { 
        messages_[index+i] = _data[i];
      }
      index += bytes_transferred;
  }

  bool check_buf()
  {
    bool flag = false;
    for(size_t i = 0; i < index; i++)
    { 
      if((messages_[i] == '\\' && messages_[i+1] == 'n') || (messages_[i] == 'n' && messages_[i-1] == '\\') || messages_[i] == '\n')
      {
        if(incoming_message != "")
        {
          myqueue.push(incoming_message);

          incoming_message = "";
          flag = true;
        }
      }
      else
      {
        incoming_message += messages_[i];
      }
    }
    if(flag)
      reset();
    else
      incoming_message = "";

    return flag;
  }

private:
  enum { max_length = 1024 };
  char messages_[max_length];
  size_t index;
  std::queue<std::string> myqueue;
  std::string incoming_message;

  void reset()
  {
    index = 0;
  }
};


/**
  * server's representation of a client socket. Server may contain multiple client sockets.
  *
  **/
class client_representative
{
  friend class server;

public:

  /**
    * Constructor for a client session.
    */
  client_representative(boost::asio::io_service& io_service, std::set<std::string>& validUsers,
    std::map<std::string, tcp::socket*>& sockets, std::map<std::string, std::string>& sockSpreadsheets,
    std::map<std::string, std::set<std::string> >& spreadSockets, std::map<std::string, std::string>& usernames, 
	std::map<std::string, spreadsheet*>& spreadsheets, std::vector<client_representative*>& connectedClients)
    : socket_(io_service),
      validUsers_(validUsers),
      sockets_(sockets),
      sockSpreadsheets_(sockSpreadsheets),
      spreadSockets_(spreadSockets),
      usernames_(usernames),
	  spreadsheets_(spreadsheets),
	  connectedClients_(connectedClients)
  {
  }

  /**
   * TODO
   */
  tcp::socket& socket()
  {
    return socket_;
  }

  /**
    * Starts the clients session, and listens for incoming bytes.
  **/
  void start(messages_pocket * pocket)
  {
    pocket_ = pocket;

    intptr_t socketIDint(reinterpret_cast<intptr_t>(&socket_));
    std::stringstream ss;
    ss << socketIDint;
    socketID = ss.str();

    sockets_[socketID] = &socket_;
    Begin_Recieve(socket_);
  }

private:

  /*
  * Handles all incoming messages from clients
  * which have been parsed into a string and contain the message only
  * without an appended '\n' character
  * 
  * Deals with 'Register', 'Connect', 'Cell' and 'Undo' commands
  */
  void message_received(const boost::system::error_code& error,
      size_t bytes_transferred)
  {
    if (!error)
    {

      pocket_->push_str(data_, bytes_transferred);


      // We check the incoming message using the data array.
      if(pocket_->check_buf())
      {
        while(!(pocket_->myqueue.empty()))
        {
          std::string msg = pocket_->myqueue.front();

          std::cout << "Recieved Command was: " << msg << std::endl;
          std::size_t positionOfSpace = msg.find(" ");
          std::string command = msg.substr(0, positionOfSpace);

          if(command == "register")
          {
            // "register client_name "
            std::string clientName = msg.substr(9);

            register_cmd(clientName, socketID);
            std::cout << "Client Name: " << clientName << std::endl;
            std::cout << "Number of Clients is: " << validUsers_.size() << std::endl;
          }
          else if(command == "connect")
          {
            // "connect client_name spreadsheet_name "
            std::string restOfMessage = msg.substr(8);
            std::size_t positionOfSpace = restOfMessage.find(" ");
            std::string clientName = restOfMessage.substr(0, positionOfSpace);
            std::string spreadName = restOfMessage.substr(positionOfSpace + 1);
            std::cout << "Client Name: " << clientName << std::endl;
            std::cout << "SpreadSheet Name: " << spreadName << std::endl;
        
            connect_cmd(clientName, spreadName, socketID);
            //std::cout << "Size of Spreadsheet: " << spreadName << " is: " << sockets_[spreadName].size() << std::endl;  // TODO: DELETEME
          }
          else if(command == "cell")
          {
            // "cell cell_name cell_contents"
            std::string restOfMessage = msg.substr(5);
            std::size_t positionOfSpace = restOfMessage.find(" ");
            std::string cellName = restOfMessage.substr(0, positionOfSpace);
            std::string cellContents = restOfMessage.substr(positionOfSpace + 1);

           // std::cout << "Cell Name: " << cellName << std::endl;  // TODO: DELETEME
           // std::cout << "Cell Content: " << cellContents << std::endl;   // TODO: DELETEME
            set_client_cell_cmd(cellName, cellContents, socketID);
          }
          else if(command == "undo")
          {
           // std::cout << "Undo Request" << std::endl;
            // "undo"
            undo_cmd(socketID);
          }
          else
          {
           // std::cout << "Invalid Command!" << std::endl;

            Begin_Send("error 2 " + msg + '\n', get_socket(socketID));
            /*  // TODO: DELETEME
            // For testing Purposes: Sending "Inavlid Command" to all connected client!
            for(std::vector<tcp::socket*>::iterator it = connectedClients_.begin(); it != connectedClients_.end(); ++it)
            {
              Begin_Send("Invalid Command!" + '\n', *(*it));
            }
            */
          }
          pocket_->myqueue.pop();
        }
      }
      else
      {
		// TODO: DELETEME
        Begin_Send("Receiving..." + '\n', get_socket(socketID));
        //sBegin_Send("Receiving...", get_socket(socketID));
      }
	    Begin_Recieve(socket_);

    }
    else
    {
      std::cout << "Server message delivery failed!" << std::endl;
      delete_client(socketID);
      free();
    }
  }

  /**
    * Function that handles sending bytes from the server socket to the client socket.
  **/
  void message_sent(const boost::system::error_code& error)
  {
    if (error)
    {
      std::cout << "Client message delivery failed!" << std::endl;
	  delete_client(socketID);
      free();
    }
  }

  /**
  * If the client calling the register command is a valid User,
  * adds the given clientName to the list of valid users
  */
  void register_cmd(std::string clientName, std::string socketID)
  {  
      // If the SocketID is associated with a valid user...
      if(std::find(validUsers_.begin(), validUsers_.end(), usernames_[socketID]) != validUsers_.end())
      {   
		  // Ensure clientName is not already registered
		  if(std::find(validUsers_.begin(), validUsers_.end(), clientName) == validUsers_.end())          
	         validUsers_.insert(clientName);     // Register clientName
          else{
             Begin_Send("error 4 " + clientName + '\n', get_socket(socketID));    // Username is already in use, 
			 return;														  // send error message 
		  }      
	  }
      else{
         Begin_Send("error 3 Current user not allowed to register users" + '\n', get_socket(socketID));
		 return;
	  }
	  // TODO: delete after testing
      Begin_Send("Registered " + clientName + '\n', get_socket(socketID));
  }


    /**
  * If username is valid, 
  * sends to the user an existing spreadsheet stored under the 
  * given username. If no existing spreadsheet is found, a new one is 
  * created with the given name and is returned to the user.
  *
  * NOTE: Before returning the spreadsheet to the user, this method returns
  * a 'Confirmed Connection' command indicating a successful connection to 
  * a spreadsheet and the number of cells in the spreadsheet to be sent.
  */
  void connect_cmd(std::string clientName, std::string spreadName, std::string socketID)
  {
		// If clientName is not a valid user, ignore message
		if(std::find(validUsers_.begin(), validUsers_.end(), clientName) == validUsers_.end())
		{
		  Begin_Send("error 4 " + clientName + '\n', get_socket(socketID));
		  return; 
		}

		// If valid, keep track of spreadsheet and socket for this user
		spreadSockets_[spreadName].insert(socketID);

		if(!sockSpreadsheets_.count(socketID)){
			sockSpreadsheets_[socketID] = spreadName;
		}

		if(!spreadsheets_.count(spreadName))
			spreadsheets_[spreadName] = new spreadsheet(spreadName);
		std::cout << "spreadsheet name " << spreadName << " should be " << (spreadsheets_[spreadName])->getName() << std::endl;

		if(!usernames_.count(socketID)) 	// TODO: uncomment this (and below) once done testing multi-clients thru same user socket
			usernames_[socketID] = clientName;
		else{
			Begin_Send("error 3 Cannot connect with different username on same socket in a single session" + '\n', get_socket(socketID));
			return;
		}

		// Send ConfirmConnection: "connected 'number_of_cells' "
		int cellCount = (spreadsheets_[spreadName])->cellCount();
	    Begin_Send(appendNum("connected ", cellCount) + '\n', get_socket(socketID));
 
		// Begin sending all cell contents to user
		SendAllCells(spreadName, socketID);
  }
	
	/**
	 * Helper method which appends strings and integers into a string
	 */
	std::string appendNum(std::string original, int num){
	  return original + boost::lexical_cast<std::string>(num);
	}


  /** 
  * If requested change to given cell is valid for the associated spreadsheet, sends the
  * requested cell change to all clients editing the current spreadsheet 
  */
  void set_client_cell_cmd(std::string cellName, std::string cellContents, std::string socketID)
  {
     
		// If user not connected to a spreadsheet, send an error
		if ( sockSpreadsheets_.find(socketID) == sockSpreadsheets_.end() ){
			Begin_Send("error 3 User is not allowed to edit without being connected to a spreadsheet" + '\n', get_socket(socketID));
			return;
		}
		// Otherwise, get spreadsheet name
		std::string spreadName = sockSpreadsheets_[socketID];
		//std::cout << "set_client_cell_cmd: spreadname is " << spreadName << std::endl;  // TODO: DELETEME
	  
		// Get a reference to the Server's copy of the spreadsheet
		spreadsheet& currSpread = *(spreadsheets_[spreadName]);
		//std::cout << "spreadName from spreadsheets_ is " << currSpread.getName() << std::endl;	// TODO: DELETEME

		// Try to set the cell contents
		if ( currSpread.setCell(cellName, cellContents) ){   // No circ dependencies occured
		
			// Iterate through all client socketIDs connected to this spreadsheet and send the associated socket 
			// the same 'cell' commandSe
			for(std::set<std::string>::iterator it = spreadSockets_[spreadName].begin(); it != spreadSockets_[spreadName].end(); ++it)
			{			  
				Begin_Send("cell " + cellName + " " + cellContents + '\n', get_socket(*it));  // TODO
			}

	  	}
		else{
			Begin_Send("error 1 Cannot create a Circular Dependency" + '\n', get_socket(socketID));  // TODO
			return;
		}
  }

 /**
  * Undoes the last change made to the associated spreadsheet
  */
  void undo_cmd(std::string socketID)
  {
    // Get spreadsheet name
    std::string spreadName = sockSpreadsheets_[socketID];
  
    // Undo most recently saved change to the server's copy of the current spreadsheet
	std::string cellInfo = (spreadsheets_[spreadName])->undo();
//std::cout << "cellInfo from undo : " << cellInfo << std::endl;
	
    // Don't allow UNDO functionality if no cell changes have been made since 
	// Server opened
	if(cellInfo.compare("") == 0){
		std::cout << "don't allow undo " << std::endl;
		return;
	}
	std::size_t positionOfSpace = cellInfo.find(" ");
    std::string cellName = cellInfo.substr(0, positionOfSpace);
    std::string cellContents = cellInfo.substr(positionOfSpace + 1);	


    // For all clients connected to this spreadsheet, send them the same SetCell command
	for(std::set<std::string>::iterator it = spreadSockets_[spreadName].begin(); it != spreadSockets_[spreadName].end(); ++it){
		Begin_Send("cell " + cellName + " " + cellContents + '\n', get_socket(*it)); 
	}
  }

 /**
  * Iterates through all cells in the given spreadsheet and sends them individually
  * in a 'cell cell_name cell_contents' commands to the given socket
  */
 void SendAllCells(std::string spreadsheetName, std::string socketID)
{
	  std::vector<spreadsheet::cellInfo> cells = (spreadsheets_[spreadsheetName])->cells();
	  std::string cellVal = "";
	  std::string cellCont = "";

	  // Iterate through all cells and send BeginSend("cell cell_name cell_contents")
	  // to the given socket for all cells in the specified spreadsheet
		//unsigned int milliseconds = 1000;
	  std::vector<spreadsheet::cellInfo>::iterator it;
	  for ( it = cells.begin(); it != cells.end(); it++ ){
			cellVal = it->name;
			cellCont = it->contents;
		
			Begin_Send("cell " + cellVal + " " + cellCont + '\n', get_socket(socketID));
			
			for(int i = 0; i < 100000000; i++){}
			//std::sleep(1);
			Begin_Send("cell " + cellVal + " " + cellCont, get_socket(socketID));
	  }
 }

/**
 * Deletes client socket information
 */
void delete_client(std::string socketID){
	
	std::string spreadName = sockSpreadsheets_[socketID];
/*
	std::cout << "socketID is " << socketID << std::endl;

	std::cout << "spreadSockets contains " << spreadSockets_.size() << " items." << std::endl;
	std::cout << "spreadSockets at " << spreadName << " contains " << spreadSockets_[spreadName].size() << " items." << std::endl;
	std::cout << "sockSpreadsheets contains " << sockSpreadsheets_.size() << " items." << std::endl;
	std::cout << "sockSpreadsheets contains " << sockSpreadsheets_[socketID] << " as spreadName." << std::endl;
	std::cout << "sockets contains " << sockets_.size() << " items." << std::endl;
	std::cout << "usernames contains " << usernames_.size() << " items." << std::endl;

	*/
	sockets_.erase(socketID); // remove socket for disconnected client

	sockSpreadsheets_.erase(socketID);	// remove spreadsheet name for disconnected client
	spreadSockets_[spreadName].erase(socketID);	  // remove socketID from spreadsheet's list of socketIDs
	
	usernames_.erase(socketID);

/*

	std::cout << "spreadSockets contains " << spreadSockets_.size() << " items." << std::endl;
	std::cout << "spreadSockets at " << spreadName << " contains " << spreadSockets_[spreadName].size() << " items." << std::endl;
	std::cout << "sockSpreadsheets contains " << sockSpreadsheets_.size() << " items." << std::endl;
	std::cout << "sockets contains " << sockets_.size() << " items." << std::endl;
	std::cout << "usernames contains " << usernames_.size() << " items." << std::endl;
*/

}

  /**
   * TODO
   */
  void Begin_Send(std::string cmd_ToBeSent, tcp::socket& socket_)
  {
    // Sending to client  
    boost::asio::async_write(socket_,
      boost::asio::buffer(cmd_ToBeSent.c_str(), cmd_ToBeSent.size()),
      boost::bind(&client_representative::message_sent, this,
        boost::asio::placeholders::error)); // Async_write is underlying socket's begin send functionality.
  }

  void enqueMessage(std::string msg, std::string socketID){

  	// Queue containing messages to be sent
	//messageToSend msgToEnqueue = messageToSend(msg, socketID);
	messagesToBeSent.push(msgToEnqueue);
  }

  void Send_Queued_Messages(){

	if(messagesToBeSent.size() > 0){
	   Begin_Send((messagesToBeSent.front()).message, get_socket((messagesToBeSent.front()).socketID);
	   messagesToBeSent.pop();
	}
	
    	boost::thread t(Send_Queued_Messages(), &(*this));
  }	

  /**
   * TODO
   */
  void Begin_Recieve(tcp::socket& socket_)
  {
    socket_.async_read_some(boost::asio::buffer(data_, max_length),
      boost::bind(&client_representative::message_received, this,
        boost::asio::placeholders::error,
        boost::asio::placeholders::bytes_transferred)); // Async_read_some is underlying socket's begin receive functionality.
  }

  /**
   * TODO
   */
  tcp::socket& get_socket(std::string id)
  {
    return *(sockets_[id]);
  }

  /**
   * TODO
   */
  void free()
  {
    std::cout << "Freeing client resources..." << std::endl;
    //socket_.shutdown(tcp::socket::shutdown_both);
    std::cout << "Shutdown successful" << std::endl;
    socket_.close();
    std::cout << "Socket closed" << std::endl;
    delete pocket_;	
    std::cout << "Deleted pocket" << std::endl;

    for(std::vector<client_representative*>::iterator it = connectedClients_.begin(); it != connectedClients_.end(); ++it)
    {
	  // Check the connectedClient 
	  if(this == (*it)){
		connectedClients_.erase(it);
		break;
	  }
    }

    delete this;
  }

  tcp::socket socket_;

  // Keeps track of all valid Usernames, defaultly containing "sysadmin"
  std::set<std::string>& validUsers_;

  // Maps [socket ids] = socket objects
  std::map<std::string, tcp::socket*>& sockets_; 

  // Maps [socket ids] = spreadsheet names    // This allows a user to login to multiple spreadsheets
  std::map<std::string, std::string>& sockSpreadsheets_;

  // Maps [spreadsheet names] = set of socket ids
  std::map<std::string, std::set<std::string> >& spreadSockets_;

  // Maps [socket ids] = usernames
  std::map<std::string, std::string>& usernames_;

  // Maps [spreadsheet names] = spreadsheet objects
  std::map<std::string, spreadsheet*>& spreadsheets_;

  // testng puposes
  std::vector<client_representative*>& connectedClients_;

  // socketID initialization
  std::string socketID;

  //pocket pointer
  messages_pocket * pocket_;

  enum { max_length = 1024 };
  char data_[max_length];
};

/**
  * Server class that accepts messages sent to it from the client, and
  * echoes them back to the client.
  **/
class server
{
public:
  server(boost::asio::io_service& io_service, short port)
    : io_service_(io_service),
      acceptor_(io_service, tcp::endpoint(tcp::v4(), port))
  {
	// Read valid users from stored file
	getValidUsersFromFile();
    begin_accept_socket();
  }

  /**
   * TODO
   */
  void close()
  {
    io_service_.post(boost::bind(&server::server_close, this));
  }


private:


  /**
   * Fills the validUsers_ structure with all previous valid users 
   * saved to file from the last time the server was opened
   */
  void getValidUsersFromFile(){

	std::string fileName = "validUsersFile.txt";

	if(fileExists(fileName)){
		
		std::cout << "_______________Using old validUsers file" << std::endl;
		std::ifstream in(fileName.c_str(), std::ifstream::in);
		std::string str;
		std::list<std::string> command_params;

		// Read the file in line by line
		std::string word;
		while(getline(in, word))
		{
		  // If the read failed, we're probably at end of file
		  //   (or else the disk went bad).  Exit the loop.
		  if (in.fail())
			break;

		  std::cout << "GetValidUsers word:  " << word << std::endl;			
		  validUsers.insert(word);

	   	}

		std::cout << "All names in validUsers_" << std::endl;
		for(std::set<std::string>::iterator it = validUsers.begin(); it != validUsers.end(); ++it)
		{
		  std::cout << "\t" << *it << std::endl;
		}

	   	// Close the file.
	   	in.close();
	}
	else{
		// Make a new file on server's first startup
		std::ofstream file(fileName.c_str());
		// Save "sysadmin" as default validUser
	    validUsers.insert("sysadmin");
		std::cout << "_______________Made new validUsers file" << std::endl;
	}
  }

  /**
   * Checks to see if a file exists
   */
  bool fileExists(const std::string& filename)
  {
	struct stat buf;
	if (stat(filename.c_str(), &buf) != -1)
	{
	    return true;
	}
	return false;
  }

  /**
   * Stores all valid users to file contained in the validUsers_ structure
   */
  void storeValidUsersToFile(){
	
	std::cout << "In storeValidUsers" << std::endl;

	std::ofstream myfile;
	myfile.open("validUsersFile.txt", std::ofstream::in | std::ofstream::out);
	if(myfile.is_open())
	{
		std::cout << "In if of storeValidUsers" << std::endl;
		for(std::set<std::string>::iterator it = validUsers.begin(); it != validUsers.end(); ++it)
    	{
			std::cout << "contained in validUsers: " << *it << std::endl;
			myfile << *it << "\n";
		}
		myfile.close();
	}
	else std::cout << "Unable to open validUsersFile";
  }

  /**
    * Begins the process of connecting the incoming socket to the server.
  **/
  void begin_accept_socket()
  {
    client_representative* new_representative = new client_representative(io_service_, validUsers, 
      sockets, sockSpreadsheets, spreadSockets, usernames, spreadsheets, connectedClients);
    acceptor_.async_accept(new_representative->socket(),
        boost::bind(&server::connection_received, this, new_representative,
          boost::asio::placeholders::error)); // Async_accept handles accepting incoming sockets. 
                        // Calls connection_received (the callback) to connect the socket to the server.
  }

  /**
    * Connects the incoming socket to the server, so long as no errors occur during this period.
  **/
  void connection_received(client_representative* new_representative,
      const boost::system::error_code& error)
  {
    if (!error)
    {
      connectedClients.push_back(new_representative);
      new_representative->start(new messages_pocket()); // Client connected successfully!
    }
    else
    {
      delete new_representative; // An error occurred, close the session.
    }

    begin_accept_socket(); // Ready to receive another connection attempt
  }

  /**
   * TODO
   */
  void server_close()
  {
    std::cout << "Disconnecting clients..." << std::endl;
    // For testing Purposes: Sending "Inavlid Command" to all connected client!
    for(std::vector<client_representative*>::iterator it = connectedClients.begin(); it != connectedClients.end(); ++it)
    {
	  // Check the connectedClient 
	  if( *it != NULL)
	      (*it)->free();
    }

    std::cout << "All clients are disconnected" << std::endl;
	storeValidUsersToFile();
	std::cout << "in server_close after storing valid users to file" << std::endl;
    io_service_.stop();
  }


  // Keeps track of all valid Usernames, defaultly containing "sysadmin"
  std::set<std::string> validUsers;

  // Maps each Spreadsheet name to a set containing all corresponding sockets
  std::map<std::string, tcp::socket*> sockets;

  //Cleaning puposes
  std::vector<client_representative*> connectedClients;

  // Maps [socket ids] = spreadsheet names    // This allows a user to login to multiple spreadsheets
  std::map<std::string, std::string> sockSpreadsheets;

  // Maps [spreadsheet names] = set of socket ids
  std::map<std::string, std::set<std::string> > spreadSockets;

  // Maps [socket ids] = usernames
  std::map<std::string, std::string> usernames;

  // Maps [spreadsheet names] = spreadsheet objects
  std::map<std::string, spreadsheet*> spreadsheets;

  boost::asio::io_service& io_service_;
  tcp::acceptor acceptor_;
};


/**
  * Builds a server on port 2014.
  **/
int main(int argc, char* argv[])
{
  try
  {
    enum { max_length = 1024 };
    char line[max_length];

    int port;
    if (argc == 2)
    {
      port = atoi(argv[1]);
    }
    else if(argc == 1)
    {
      port = 2000;
    }
    else
    {
      std::cerr << "Usage: async_tcp_echo_server <port>\n";
      return 1;
    }

    boost::asio::io_service io_service;
    server s(io_service, port); 
    boost::thread t(boost::bind(&boost::asio::io_service::run, &io_service));
    
    while (std::cin.getline(line, max_length))
    {
      std::string str(line, strlen(line));
      if(str == "q")
      {
        break;
      }
    }

    s.close();
    t.join();

  }
  catch (std::exception& e)
  {
    std::cerr << "Exception: " << e.what() << "\n";
    return 1;
  }

  return 0;
}

