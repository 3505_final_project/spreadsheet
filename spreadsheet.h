#ifndef SPREADSHEET_H
#define SPREADSHEET_H

// Includes are in alphabetical order
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include <iostream>
#include <sqlite3.h>
#include <string>
#include <vector>
#include <map>
#include <set>
#include <stack>
#include <ctype.h>
#include <stdint.h>

/** A spreadsheet that is saved to a sqlite database
 */
class spreadsheet{
public:
  /** Creates a new spreadsheet with the given filename.
   * If the file does not exist it is created.
   * If the file does exist the spreadsheet data is taken from the file.
   */
  spreadsheet(std::string, bool consoleLog=true);

  bool operator==(spreadsheet&);
  
  std::string getName();

  /** Deletes all the cells from this spreadsheet
   */
  void clear();

  /** Returns the contents of the cell with the given name
   */
  std::string cell(std::string);

  /** Returns the number of Cells in the current spreadsheet
   */
  int cellCount();
  
  /** Sets the contents of a cell.
   * @param name the name of the cell to modify.
   * @param contents the contents of the cell being modified.
   */
  bool setCell(std::string, std::string);

  // cells returns a vector<cellInfo>
  struct cellInfo{
      std::string name;
      std::string contents;
  };

  /** Returns a vector containing all the cells contained in this spreadsheet.
   */
  std::vector<cellInfo> cells();

  /**
	* Undoes the last change made to the current spreadsheet and returns the command
	* used to revert back to the old contents
	*/
  std::string undo();

  /** Closes this spreadsheet
   */
  void close();

private:
  sqlite3* db;
    // The database where the spreadsheet is saved
  std::string name;
    // The name of the spreadsheet
  bool consoleLog;
  std::map<std::string, std::set<std::string> > dependentsMap;
    // Maps cellName to set of dependent cells ( by cellName)
    // to keep track of cell dependencies
  std::stack<std::string> prevCellValues;

  /** Sets the contents of cell in the database to the given contents
   */
  void updateCell(std::string, std::string);

  /** Returns true if the database has a cell with the given name
   */
  bool containsCell(std::string);

  /** Adds a cell to the database with the given name and contents
   */
  void insertCell(std::string, std::string);

  /** Removes the cell from the database with the given name
    */
  void removeCell(std::string);

  /**
   * Stores all cellNames in the given contents into a set and returns the set
   */
  std::set<std::string> parseCellContents (std::string cellContents);
 
  /**
   * Determines if a cell has circular dependencies
   */
  bool findCycle(std::string cellName, std::set<std::string> dependents);

  /** 
   * Recursive helper method for findCycle to determine if a cell has circular dependencies
   *
   * Citation: Idea and structure reference by Stack Overflow at 
   * :http://stackoverflow.com/questions/19113189/detecting-cycles-in-a-graph-using-dfs-2-different-approaches-and-whats-the-dif
   */
  void findCycleRecursive(std::string cellName, std::set<std::string> dependents, 
			  std::map<std::string, bool> &visited, std::map<std::string, bool> &onStack,
			  bool &hasCycle);

  /**
   * Adds the dependencies for the given cellName 
   * and all cellNames in the set dependents
   */
  void addDependents (std::string cellName, std::set<std::string> dependents);

  /** Logs the string to the console if the logger is turned on
   */
  void log(std::string);
};

/**
 * Helper method that appends the given number to the end of the string
 */
std::string appendNum(std::string original, int num);
#endif
