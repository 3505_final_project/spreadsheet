#include "spreadsheet.h"
using namespace std;

/** Tests the spreadsheet class 
 * by inserting and removing values using only public methods.
 * Prints out what the db should hold at the end.
 */
void test(){
  
  //--------TEST DEPENDENCIES--------

  spreadsheet sDep("test.db");
  sDep.clear();

  // Test good cases
  if(!sDep.setCell("Y1", "Y2"))
		cout << "Dependency Error: No exception should have been caused for 'Y1 = Y2'" << endl;
  if(!sDep.setCell("Y1", "Y2 + Y3 * (Y5 / Y6)"))
		cout << "Dependency Error: No exception should have been caused for 'X1 = Y2 + Y3 * (Y5 / Y6)'" << endl;

  // Test bad cases
  if(sDep.setCell("X1", "=X1"))
	cout << "Dependency Error: Circular Dependency should have prevented setCell from executing for 'X1 = X1'" << endl;
  if(sDep.setCell("X1", "=X2 + (X3 * X1)"))
	cout << "Dependency Error: Circular Dependency should have prevented setCell from executing for 'X1 = X2 + (X3 * X1)'" << endl;
  sDep.setCell("X1", "=X2");
  sDep.setCell("X2", "=X3 + X4");
  if(sDep.setCell("X3", "=X5 * X1"))
	cout << "Dependency Error: Circular Dependency should have prevented setCell from executing for 'X3 = X5 * X1'" << endl;
	sDep.close();
	cout << "Done testing sDep" << endl;
	
  //-------TEST GENERAL CELL FUNCTIONALITY--------

  // Test the cell method

  spreadsheet s("test.db");
  s.clear();

  s.setCell("A1", "Happy Day");
  s.setCell("Z1", "  ");
  s.setCell("R5", "We are being graded");

  cout << endl;
  string a1 = s.cell("A1");
  if(a1 != "Happy Day")
    cout << "A1 should contain Happy Day, but instead it contains " << a1 << endl;
  string a5 = s.cell("A5");
  if(a5 != "")
    cout << "A5 should be empty, but it contains " << a5 << endl;
  string z1 = s.cell("Z1");
  if(z1 != "")
    cout << "Z1 should be empty, but it contains " << z1 << endl;
  string r5 = s.cell("R5");
  if(r5 != "We are being graded")
    cout << "R5 should say 'We are being graded, but it contains " << r5 << endl;

  // Test the cells method
  vector<spreadsheet::cellInfo> cells = s.cells();
  if(cells.size() != static_cast<uint8_t>(s.cellCount()))
    cerr << "The cellCount method is broken" << endl;
  if(cells.size() == 2){
    if((cells[0].name != "A1" && cells[0].contents != "Happy Day") && (cells[1].name != "A1" && cells[1].contents != "Happy Day"))
      cerr << "Cell A1 with contents A1 was not found in the spreadsheet" << endl;

    if((cells[0].name != "R5" && cells[0].contents != "We are being graded") && (cells[1].name != "R5" && cells[1].contents != "We are being graded"))
      cerr << "Cell R5 with contents R5 was not found in the spreadsheet" << endl;
  }
  else
    cerr << "There should only be two cells in the database, but there are " << cells.size() << endl;    

  cout << "Stress Testing" << endl;
  cout << "Adding cells" << endl;
  s.clear();
  for(int num = 1; num < 101; num++){
    string name = appendNum("cell ", num);
    string contents = appendNum("contents ", num);
    s.setCell(name, contents);
  }
  cout << "Updating cells" << endl;
  for(int num = 1; num < 101; num++){
    string name = appendNum("cell ", num + 10);
    string contents = appendNum("contents ", num);
    s.setCell(name, contents);
  }
  cout << "Removing cells" << endl;
  for(int num = 1; num < 101; num++){
    string name = appendNum("cell ", num);
    string contents = "";
    s.setCell(name, contents);
  }
	cout << "Final test, adding 1000 cells" << endl;
	for(int num = 0; num < 1000; num++){
		string name = "cell " + num;
		string contents = "contents " + num;
		s.setCell(name, contents);
	}


  cout << "Done stress testing" << endl;

  s.close();
}

void cells(){
  //-------TEST CELLS() METHOD WORKING CORRECTLY-----

  spreadsheet sCells("test.db");
  sCells.clear();

  sCells.setCell("A1", "=A3");
  sCells.setCell("A1", "=A1");


  std::vector<spreadsheet::cellInfo> cells_t = sCells.cells();
  std::string cellVal = "";
  std::string cellCont = "";

  // Iterate through all cells and send BeginSend("cell cell_name cell_contents")
  // to the given socket for all cells in the specified spreadsheet

  std::vector<spreadsheet::cellInfo>::iterator it;
  for ( it = cells_t.begin(); it != cells_t.end(); it++ ){
        cellVal = it->name;
        cellCont = it->contents;
    
        std::cout << "cell is " << cellVal << "  " << cellCont << std::endl;
  }
  sCells.close();
}


int main(int argc, char* argv[]){
	spreadsheet sprd("test.db");
	sprd.clear();
	sprd.setCell("A1", "5");
	sprd.setCell("A1", "10");
	sprd.close();

	test();
    cells();
}
