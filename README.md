# README #

### What is this repository for? ###
Storing all files necessary for a spreadsheet file and server to communicate in the protocol specified by 
the Axis of Ignorance Version 2 Protocol for CS 3505, taught by Peter Jensen.

### How do I get set up? ###

Starting the Server:
     In the directory ServerSocket, execute the command 'make server'. Then execute ./a.out <PORT #>.

--- Basic Functionality Testing ---
Starting the Client
     In the directory ServerSocket, execute the command 'make client'. Then execute ./a.out <IP Address> <PORT #>.

NOTE: You must start the Server before starting the client. The PORT # for both the Server and the Client must match.

--- Actually running the Spreadsheet Application ---
Run the SpreadsheetGUI application for the Solution Spreadsheet in Visual Studios.

### Contribution guidelines ###

Refer to the Hansoft Project CS 3505 for Group 'A Hundred Bucks'

### Who do I talk to? ###

* Repo Owner: Ryan Kingston
* Group Members: Ryan Kingston, Joshua Oblinsky, Fahad Tmem, Taylor Jenkins