    
#include <iostream>
#include <string>


 bool isVariable(std::string s)
{

	bool valid = true;
	bool isInt = false;
	bool isAlpha = false;

    for (int i = 0; i < s.length(); i++)
    {
        if (!isInt)
        {
			if(isalpha(s[i])){
				isAlpha = true;
				continue;
			}
			else if(isdigit(s[i]))
				isInt = true;
			else{
	            valid = false;
				break;
			}
        }
		else if((isInt && !isdigit(s[i]))  ){ 
			valid = false;
			break;
		}
    }
	if(valid && isInt && isAlpha)
		return true;
	else
		return false;
}

int main (){
	if(isVariable("fffff444"))
		std::cout << "isVar" << std::endl;
	else
		std::cout << "notVar" << std::endl;

}
