<?xml version="1.0"?>
<doc>
    <assembly>
        <name>Formula</name>
    </assembly>
    <members>
        <!-- Badly formed XML comment ignored for member "T:SpreadsheetUtilities.Formula" -->
        <member name="M:SpreadsheetUtilities.Formula.#ctor(System.String)">
            <summary>
            Creates a Formula from a string that consists of an infix expression written as
            described in the class comment.  If the expression is syntactically invalid,
            throws a FormulaFormatException with an explanatory Message.
            
            The associated normalizer is the identity function, and the associated validator
            maps every string to true.  
            </summary>
        </member>
        <member name="M:SpreadsheetUtilities.Formula.#ctor(System.String,System.Func{System.String,System.String},System.Func{System.String,System.Boolean})">
            <summary>
            Creates a Formula from a string that consists of an infix expression written as
            described in the class comment.  If the expression is syntactically incorrect,
            throws a FormulaFormatException with an explanatory Message.
            
            The associated normalizer and validator are the second and third parameters,
            respectively.  
            
            If the formula contains a variable v such that normalize(v) is not a legal variable, 
            throws a FormulaFormatException with an explanatory message. 
            
            If the formula contains a variable v such that isValid(normalize(v)) is false,
            throws a FormulaFormatException with an explanatory message.
            
            Suppose that N is a method that converts all the letters in a string to upper case, and
            that V is a method that returns true only if a string consists of one letter followed
            by one digit.  Then:
            
            new Formula("x2+y3", N, V) should succeed
            new Formula("x+y3", N, V) should throw an exception, since V(N("x")) is false
            new Formula("2x+y3", N, V) should throw an exception, since "2x+y3" is syntactically incorrect.
            </summary>
        </member>
        <member name="M:SpreadsheetUtilities.Formula.Evaluate(System.Func{System.String,System.Double})">
             <summary>
             Evaluates this Formula, using the lookup delegate to determine the values of
             variables.  When a variable symbol v needs to be determined, it should be looked up
             via lookup(normalize(v)). (Here, normalize is the normalizer that was passed to 
             the constructor.)
             
             For example, if L("x") is 2, L("X") is 4, and N is a method that converts all the letters 
             in a string to upper case:
             
             new Formula("x+7", N, s => true).Evaluate(L) is 11
             new Formula("x+7").Evaluate(L) is 9
             
             Given a variable symbol as its parameter, lookup returns the variable's value 
             (if it has one) or throws an ArgumentException (otherwise).
             
             If no undefined variables or divisions by zero are encountered when evaluating 
             this Formula, the value is returned.  Otherwise, a FormulaError is returned.  
             The Reason property of the FormulaError should have a meaningful explanation.
            
             This method should never throw an exception.
             </summary>
        </member>
        <member name="M:SpreadsheetUtilities.Formula.GetVariables">
            <summary>
            Enumerates the normalized versions of all of the variables that occur in this 
            formula.  No normalization may appear more than once in the enumeration, even 
            if it appears more than once in this Formula.
            
            For example, if N is a method that converts all the letters in a string to upper case:
            
            new Formula("x+y*z", N, s => true).GetVariables() should enumerate "X", "Y", and "Z"
            new Formula("x+X*z", N, s => true).GetVariables() should enumerate "X" and "Z".
            new Formula("x+X*z").GetVariables() should enumerate "x", "X", and "z".
            </summary>
        </member>
        <member name="M:SpreadsheetUtilities.Formula.ToString">
            <summary>
            Returns a string containing no spaces which, if passed to the Formula
            constructor, will produce a Formula f such that this.Equals(f).  All of the
            variables in the string should be normalized.
            
            For example, if N is a method that converts all the letters in a string to upper case:
            
            new Formula("x + y", N, s => true).ToString() should return "X+Y"
            new Formula("x + Y").ToString() should return "x+Y"
            </summary>
        </member>
        <member name="M:SpreadsheetUtilities.Formula.Equals(System.Object)">
            <summary>
            If obj is null or obj is not a Formula, returns false.  Otherwise, reports
            whether or not this Formula and obj are equal.
            
            Two Formulae are considered equal if they consist of the same tokens in the
            same order.  To determine token equality, all tokens are compared as strings 
            except for numeric tokens, which are compared as doubles, and variable tokens,
            whose normalized forms are compared as strings.
            
            For example, if N is a method that converts all the letters in a string to upper case:
             
            new Formula("x1+y2", N, s => true).Equals(new Formula("X1  +  Y2")) is true
            new Formula("x1+y2").Equals(new Formula("X1+Y2")) is false
            new Formula("x1+y2").Equals(new Formula("y2+x1")) is false
            new Formula("2.0 + x7").Equals(new Formula("2.000 + x7")) is true
            </summary>
        </member>
        <member name="M:SpreadsheetUtilities.Formula.op_Equality(SpreadsheetUtilities.Formula,SpreadsheetUtilities.Formula)">
            <summary>
            Reports whether f1 == f2, using the notion of equality from the Equals method.
            Note that if both f1 and f2 are null, this method should return true.  If one is
            null and one is not, this method should return false.
            </summary>
        </member>
        <member name="M:SpreadsheetUtilities.Formula.op_Inequality(SpreadsheetUtilities.Formula,SpreadsheetUtilities.Formula)">
            <summary>
            Reports whether f1 != f2, using the notion of equality from the Equals method.
            Note that if both f1 and f2 are null, this method should return false.  If one is
            null and one is not, this method should return true.
            </summary>
        </member>
        <member name="M:SpreadsheetUtilities.Formula.GetHashCode">
            <summary>
            Returns a hash code for this Formula.  If f1.Equals(f2), then it must be the
            case that f1.GetHashCode() == f2.GetHashCode().  Ideally, the probability that two 
            randomly-generated unequal Formulae have the same hash code should be extremely small.
            </summary>
        </member>
        <member name="M:SpreadsheetUtilities.Formula.GetTokens(System.String,System.Func{System.String,System.String})">
            <summary>
            Given an expression, enumerates the tokens that compose it.  Tokens are left paren;
            right paren; one of the four operator symbols; a string consisting of a letter or underscore
            followed by zero or more letters, digits, or underscores; a double literal; and anything that doesn't
            match one of those patterns.  There are no empty tokens, and no token contains white space.
            </summary>
        </member>
        <member name="M:SpreadsheetUtilities.Formula.EvaluateDouble(System.Collections.Generic.Stack{System.Double},System.Collections.Generic.Stack{System.String},System.Double,System.Boolean)">
            <summary>
            A private helper method that evaluate an Double in the expression
            </summary>
            <param name="valueStack">Stack that contains the values</param>
            <param name="operatorStack">Stack that contains the operators</param>
            <param name="Double">The Double that needs to be evaluated</param>
            <param name="isOneIntegrGiven">helps to decide whether to pop one Double or two</param>
            <returns>False, if division by zero occurs</returns>
        </member>
        <member name="M:SpreadsheetUtilities.Formula.EvaluateNonDouble(System.Collections.Generic.Stack{System.Double},System.Collections.Generic.Stack{System.String},System.String)">
            <summary>
            A private helper that evaluates a character/operator in the expression
            </summary>
            <param name="valueStack">Stack that contains the values</param>
            <param name="operatorStack">Stack that contains the operators</param>
            <param name="str">The character/operator that needs to be evaluated</param>
        </member>
        <member name="M:SpreadsheetUtilities.Formula.isValidFormula(System.String,System.Func{System.String,System.String},System.Func{System.String,System.Boolean})">
            <summary>
            This method examine a given formula and returns True, 
            if the formula is formatted correctly; false otherwise.
            </summary>
            <param name="passedFormula">The given formula as a string</param>
            <param name="normalize">The given normelizer delegate</param>
            <param name="isValid">The given validator delegate</param>
            <returns>True if the formula is formatted correctly; false otherwise</returns>
        </member>
        <member name="M:SpreadsheetUtilities.Formula.isValidToken(System.String)">
            <summary>
            Returns true, if the given token is eaither 
            an open/closed parenthesis or an operator; false otherwise
            </summary>
            <param name="str">The given Token</param>
            <returns>true, if the given token is eaither 
            an open/closed parenthesis or an operator; false otherwise</returns>
        </member>
        <member name="M:SpreadsheetUtilities.Formula.isOperator(System.String)">
            <summary>
            Returns true, if the given token is an operator
            </summary>
            <param name="str">The given Token</param>
            <returns>true, if the given token is an operator</returns>
        </member>
        <member name="M:SpreadsheetUtilities.Formula.strToDouble(System.String)">
            <summary>
            This method converts a double number to its standard formatt
            </summary>
            <param name="str">The given Token, that could be a double</param>
            <returns>Corretly formatted double number (as string) if found</returns>
        </member>
        <member name="M:SpreadsheetUtilities.Formula.isVariable(System.String)">
            <summary>
            Private helper that decides if the given string is a standard valid variable
            </summary>
            <param name="s">The string that needs to be varified</param>
            <returns>
            True: if the string is a valid variable
            False: otherwise
            </returns>
        </member>
        <member name="T:SpreadsheetUtilities.FormulaFormatException">
            <summary>
            Used to report syntactic errors in the argument to the Formula constructor.
            </summary>
        </member>
        <member name="M:SpreadsheetUtilities.FormulaFormatException.#ctor(System.String)">
            <summary>
            Constructs a FormulaFormatException containing the explanatory message.
            </summary>
        </member>
        <member name="T:SpreadsheetUtilities.FormulaError">
            <summary>
            Used as a possible return value of the Formula.Evaluate method.
            </summary>
        </member>
        <member name="M:SpreadsheetUtilities.FormulaError.#ctor(System.String)">
            <summary>
            Constructs a FormulaError containing the explanatory reason.
            </summary>
            <param name="reason"></param>
        </member>
        <member name="P:SpreadsheetUtilities.FormulaError.Reason">
            <summary>
             The reason why this FormulaError was created.
            </summary>
        </member>
    </members>
</doc>
