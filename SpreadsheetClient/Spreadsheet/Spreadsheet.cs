﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpreadsheetUtilities;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Linq;
using System.Net.Sockets;
using SS;
using System.Net;
using Spreadsheet;

namespace SS
{
    /// <summary>
    /// A class that represents a Spreedsheet with cells
    /// </summary>
    /// <modifier>Fahad A Alothaimeen</modifier>
    /// <uNID>U0824563</uNID>
    public class Spreadsheet : AbstractSpreadsheet
    {
        // ADDED FOR PS5
        /// <summary>
        /// True if this spreadsheet has been modified since it was created or saved                  
        /// (whichever happened most recently); false otherwise.
        /// </summary>
        public override bool Changed { get; protected set; }

        /// <summary>
        /// This is a dictinary that represent a spreadsheet,
        /// A spreadsheet is a collection of cells that could
        /// depend on each other.
        /// 
        /// A cell name is the kay, and the cell object is the value
        /// </summary>
        private Dictionary<string, Cell> Cells;

        /// <summary>
        /// Dpendency Graph to represent the 
        /// dependencies between cells if any
        /// </summary>
        private DependencyGraph Graph;

        /// <summary>
        /// Object that sends and receives all messages to and from the server
        /// </summary>
        private Communicator serverComm;

        /// <summary>
        /// Expected number of cells to be received by the server.
        /// </summary>
        private int expectedCells;


        /// <summary>
        /// Creats an empty Spreadsheet
        /// </summary>
        public Spreadsheet() :
            this(s => true, s => s, "default")
        {
        }

        // ADDED FOR PS5
        /// <summary>
        /// Constructs an override spreadsheet by recording its variable validity test,
        /// its normalization method, and its version information.  The variable validity
        /// test is used throughout to determine whether a string that consists of one or
        /// more letters followed by one or more digits is a valid cell name.  The variable
        /// equality test should be used thoughout to determine whether two variables are
        /// equal.
        /// </summary>
        public Spreadsheet(Func<string, bool> isValid, Func<string, string> normalize, string version) :
            base(isValid, normalize, version)
        {
            this.IsValid = isValid;
            this.Normalize = normalize;
            this.Version = version;
            Changed = false;

            Cells = new Dictionary<string, Cell>();
            Graph = new DependencyGraph();


            // TODO: set the hostname and port to the actual ones that will be used
         /*   serverComm = new Communicator("lab1-3.eng.utah.edu", 80);
            serverComm.ConfirmConn += ConfirmCommunication;
            serverComm.SetClientCell += SetContentsOfCellHelper;
            serverComm.Error += HandleErrors;
            */
        }

        /** Constructor to connect the spreadsheet to a server.  
         * This is the only constructor that initializes the connection to the server
         * so only this constructor can be used now.
         */
        public Spreadsheet(string hostname, int port, string username, string spreadsheetName)
            : this(s => true, s => s.ToUpper(), "default")
        {
            serverComm = new Communicator(hostname, port);
            serverComm.SetClientCell += SetClientCellHelper;
            serverComm.ConfirmConn += ConfirmConnectionHelper;
            serverComm.Error += ErrorHelper;
            serverComm.Connect(username, spreadsheetName);

        }

        /*
        // ADDED FOR PS5
        /// <summary>
        /// Constructs an override spreadsheet by recording its variable validity test,
        /// its normalization method, and its version information.  The variable validity
        /// test is used throughout to determine whether a string that consists of one or
        /// more letters followed by one or more digits is a valid cell name.  The variable
        /// equality test should be used thoughout to determine whether two variables are
        /// equal.
        /// </summary>
        public Spreadsheet(string filePath, Func<string, bool> isValid, Func<string, string> normalize, string version) :
            base(isValid, normalize, version)
        {
            this.IsValid = isValid;
            this.Normalize = normalize;
            this.Version = version;

            Cells = new Dictionary<string, Cell>();
            Graph = new DependencyGraph();

            string attribute = versionChecking(filePath);
            if (version != attribute)
            { throw new SpreadsheetReadWriteException("version requested does not match"); }

            XDocument xDoc = XDocument.Load(filePath);
            var cells = xDoc.Descendants("cell").ToList();
            foreach(var cellBlock in cells)
            {
                this.SetContentsOfCell(cellBlock.Element("name").Value, 
                    cellBlock.Element("contents").Value);              
            }
            Changed = false;
        }*/


        /// <summary>
        /// Event handler for setting the client cell.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="content"></param>
        private void SetClientCellHelper(string name, string content)
        {
            ISet<String> dependencies = SetContentsOfCell(name, content);
            Recalculate(dependencies);


            if (expectedCells == 0)
                releaseClient();
            else
                expectedCells--;
        }

        /// <summary>
        /// Event handler for confirming client connection.
        /// </summary>
        /// <param name="value"></param>
        private void ConfirmConnectionHelper(int value)
        {
            expectedCells = value;
            freezeClient();
        }

        /// <summary>
        /// Event handler that handles errors.
        /// </summary>
        /// <param name="errorCode"></param>
        /// <param name="message"></param>
        private void ErrorHelper(int errorCode, string message)
        {
            viewErrorCall(errorCode, message);
        }

        // ADDED FOR PS5
        /// <summary>
        /// Returns the version information of the spreadsheet saved in the named file.
        /// If there are any problems opening, reading, or closing the file, the method
        /// should throw a SpreadsheetReadWriteException with an explanatory message.
        /// </summary>
        public override string GetSavedVersion(String filename)
        {
            string filePath;
            if (!filename.EndsWith(".xml") || !filename.EndsWith(".txt"))
            { filePath = @".\" + filename + @".xml"; }
            else { filePath = @".\" + filename; }

            return versionChecking(filePath);
        } 

        // ADDED FOR PS5
        /// <summary>
        /// Writes the contents of this spreadsheet to the named file using an XML format.
        /// The XML elements should be structured as follows:
        /// 
        /// <spreadsheet version="version information goes here">
        /// 
        /// <cell>
        /// <name>
        /// cell name goes here
        /// </name>
        /// <contents>
        /// cell contents goes here
        /// </contents>    
        /// </cell>
        /// 
        /// </spreadsheet>
        /// 
        /// There should be one cell element for each non-empty cell in the spreadsheet.  
        /// If the cell contains a string, it should be written as the contents.  
        /// If the cell contains a double d, d.ToString() should be written as the contents.  
        /// If the cell contains a Formula f, f.ToString() with "=" prepended should be written as the contents.
        /// 
        /// If there are any problems opening, writing, or closing the file, the method should throw a
        /// SpreadsheetReadWriteException with an explanatory message.
        /// </summary>
        public override void Save(String filename)
        {
            try
            {
                if (Changed)
                {
                    using (XmlWriter xmlFile = XmlWriter.Create(filename))
                    {
                        xmlFile.WriteStartDocument();
                        xmlFile.WriteStartElement("spreadsheet");
                        xmlFile.WriteAttributeString("version", Version);

                        foreach (var cell in Cells)
                        {
                            xmlFile.WriteStartElement("cell");
                            xmlFile.WriteStartElement("name");
                            xmlFile.WriteString(cell.Key);
                            xmlFile.WriteEndElement();
                            xmlFile.WriteStartElement("contents");
                            xmlFile.WriteString(cell.Value.getCellContent().ToString());
                            xmlFile.WriteEndElement();
                            xmlFile.WriteEndElement();
                        }
                        xmlFile.WriteEndElement();
                        xmlFile.WriteEndDocument();
                    }
                    Changed = false;
                }
            }
            catch (Exception)
            {
                throw new SpreadsheetReadWriteException("");
            }
        }

        // ADDED FOR PS5
        /// <summary>
        /// If name is null or invalid, throws an InvalidNameException.
        /// 
        /// Otherwise, returns the value (as opposed to the contents) of the named cell.  The return
        /// value should be either a string, a double, or a SpreadsheetUtilities.FormulaError.
        /// </summary>
        public override object GetCellValue(String name)
        {
            if (name == null || !Regex.Match(name,
                @"^[a-zA-Z]{1}?[0-9]{1,2}?$").Success
                || !IsValid(name))
            {
                throw new InvalidNameException();
            }
            else
            {
                name = Normalize(name);
                // Creates a new cell if non is found with the same name
                if (!Cells.ContainsKey(name))
                {
                    Cell newCell = new Cell("");
                    Cells.Add(name, newCell);
                }
                return Cells[name].getCellValue();
            }
        }

        /// <summary>
        /// Enumerates the names of all the non-empty cells in the spreadsheet.
        /// </summary>
        public override IEnumerable<String> GetNamesOfAllNonemptyCells()
        {
            HashSet<string> nonEmptyCells = new HashSet<string>();
            foreach (var cell in Cells)
            {
                if (!cell.Value.isCellEmpty())
                {
                    nonEmptyCells.Add(cell.Key);
                }
            }
            return nonEmptyCells;
        }

        /// <summary>
        /// If name is null or invalid, throws an InvalidNameException.
        /// 
        /// Otherwise, returns the contents (as opposed to the value) of the named cell.  The return
        /// value should be either a string, a double, or a Formula.
        /// </summary>
        public override object GetCellContents(String name)
        {
            if (name == null || !Regex.Match(name,
                @"^[a-zA-Z]{1}?[0-9]{1,2}?$").Success
                || !IsValid(name))
            {
                throw new InvalidNameException();
            }
            else
            {
                name = Normalize(name);
                // Creates a new cell if non is found with the same name
                if (!Cells.ContainsKey(name))
                {
                    Cell newCell = new Cell("");
                    Cells.Add(name, newCell);
                }
                return Cells[name].getCellContent();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="content"></param>
        public override void send_cmd(String name, String content)
        {
            if (serverComm != null)
                serverComm.SetServerCell(name, content);
        }



        // ADDED FOR PS5
        /// <summary>
        /// If content is null, throws an ArgumentNullException.
        /// 
        /// Otherwise, if name is null or invalid, throws an InvalidNameException.
        /// 
        /// Otherwise, if content parses as a double, the contents of the named
        /// cell becomes that double.
        /// 
        /// Otherwise, if content begins with the character '=', an attempt is made
        /// to parse the remainder of content into a Formula f using the Formula
        /// constructor.  There are then three possibilities:
        /// 
        ///   (1) If the remainder of content cannot be parsed into a Formula, a 
        ///       SpreadsheetUtilities.FormulaFormatException is thrown.
        ///       
        ///   (2) Otherwise, if changing the contents of the named cell to be f
        ///       would cause a circular dependency, a CircularException is thrown.
        ///       
        ///   (3) Otherwise, the contents of the named cell becomes f.
        /// 
        /// Otherwise, the contents of the named cell becomes content.
        /// 
        /// If an exception is not thrown, the method returns a set consisting of
        /// name plus the names of all other cells whose value depends, directly
        /// or indirectly, on the named cell.
        /// 
        /// For example, if name is A1, B1 contains A1*2, and C1 contains B1+A1, the
        /// set {A1, B1, C1} is returned.
        /// </summary>
        public override ISet<String> SetContentsOfCell(String name, String content)
        {
            if (content == null)
            {
                throw new ArgumentNullException();
            }
            else if (name == null || !Regex.Match(name,
                @"^[a-zA-Z]{1}?[0-9]{1,2}?$").Success
                || !IsValid(name))
            {
                throw new InvalidNameException();
            }
            else
            {
          
                // Do this work if the server gives the okay to do so.
                double value;
                bool isNumber = double.TryParse(content, out value);

                ISet<string> ReCalculationSet;
                if (isNumber)
                {
                    ReCalculationSet = SetCellContents(name, value);
                }
                else if (content.Length > 0 && content[0].Equals('='))
                {
                    ReCalculationSet = SetCellContents(name, new Formula(content,
                        this.Normalize, this.IsValid));
                }
                else
                {
                    ReCalculationSet = SetCellContents(name, content);
                }
                if (ReCalculationSet.Count > 1) { Recalculate(ReCalculationSet); }

                this.update_cells(ReCalculationSet);
                return ReCalculationSet;
            }
            
        }

        /** Reverts the last cell edit made to the spreadsheet.
         */
        public override void Undo() {
            if(serverComm != null)
                serverComm.Undo();
        }

        // MODIFIED PROTECTION FOR PS5
        /// <summary>
        /// If name is null or invalid, throws an InvalidNameException.
        /// 
        /// Otherwise, the contents of the named cell becomes number.  The method returns a
        /// set consisting of name plus the names of all other cells whose value depends, 
        /// directly or indirectly, on the named cell.
        /// 
        /// For example, if name is A1, B1 contains A1*2, and C1 contains B1+A1, the
        /// set {A1, B1, C1} is returned.
        /// </summary>
        protected override ISet<String> SetCellContents(String name, double number)
        {
            if (!Regex.Match(name, @"^[a-zA-Z]{1}?[0-9]{1,2}?$").Success
                || !IsValid(name))
            {
                throw new InvalidNameException();
            }
            else
            {
                name = Normalize(name);
                Cell newCell = new Cell(number);

                // Add a new cell if name is not found
                // reAssign the cell otherwise
                if (!Cells.ContainsKey(name))
                { Cells.Add(name, newCell); }
                else { Cells[name] = newCell; }

                Changed = true;
                return new HashSet<string>(this.GetCellsToRecalculate(name));
            }
        }


        // MODIFIED PROTECTION FOR PS5
        /// <summary>
        /// If text is null, throws an ArgumentNullException.
        /// 
        /// Otherwise, if name is null or invalid, throws an InvalidNameException.
        /// 
        /// Otherwise, the contents of the named cell becomes text.  The method returns a
        /// set consisting of name plus the names of all other cells whose value depends, 
        /// directly or indirectly, on the named cell.
        /// 
        /// For example, if name is A1, B1 contains A1*2, and C1 contains B1+A1, the
        /// set {A1, B1, C1} is returned.
        /// </summary>
        protected override ISet<String> SetCellContents(String name, String text)
        {
            if (text == null)
            {
                throw new ArgumentNullException("inValid null text");
            }
            else if (!Regex.Match(name, @"^[a-zA-Z]{1}?[0-9]{1,2}?$").Success
                || !IsValid(name))
            {
                throw new InvalidNameException();
            }
            else
            {
                name = Normalize(name);
                Cell newCell = new Cell(text);

                // Add a new cell if name is not found
                // reAssign the cell otherwise
                if (!Cells.ContainsKey(name))
                { Cells.Add(name, newCell); }
                else { Cells[name] = newCell; }

                Changed = true;
                return new HashSet<string>(this.GetCellsToRecalculate(name));
            }
        }


        // MODIFIED PROTECTION FOR PS5
        /// <summary>
        /// If formula parameter is null, throws an ArgumentNullException.
        /// 
        /// Otherwise, if name is null or invalid, throws an InvalidNameException.
        /// 
        /// Otherwise, if changing the contents of the named cell to be the formula would cause a 
        /// circular dependency, throws a CircularException.
        /// 
        /// Otherwise, the contents of the named cell becomes formula.  The method returns a
        /// Set consisting of name plus the names of all other cells whose value depends,
        /// directly or indirectly, on the named cell.
        /// 
        /// For example, if name is A1, B1 contains A1*2, and C1 contains B1+A1, the
        /// set {A1, B1, C1} is returned.
        /// </summary>
        protected override ISet<String> SetCellContents(String name, Formula formula)
        {
            if (formula == null)
            {
                throw new ArgumentNullException("inValid null formula");
            }
            else if (!Regex.Match(name, @"^[a-zA-Z]{1}?[0-9]{1,2}?$").Success
                || !IsValid(name))
            {
                throw new InvalidNameException();
            }
            else
            {
                name = Normalize(name);
                IEnumerator<string> cellsNames = formula.GetVariables().GetEnumerator();
                while (cellsNames.MoveNext())
                {
                    Graph.AddDependency(cellsNames.Current, name);
                }

                HashSet<string> cellsToReCalculate;
                try
                {
                    cellsToReCalculate = new HashSet<string>(this.GetCellsToRecalculate(name));
                }
                catch (Exception)
                {
                    // Resetting dependecies
                    cellsNames.Reset();
                    while (cellsNames.MoveNext())
                    {
                        Graph.RemoveDependency(cellsNames.Current, name);
                    }
                    throw new CircularException();
                }

                Cell newCell = new Cell(formula, Cells);

                // Add a new cell if name is not found
                // reAssign the cell otherwise
                if (!Cells.ContainsKey(name))
                { Cells.Add(name, newCell); }
                else { Cells[name] = newCell; }

                Changed = true;
                return cellsToReCalculate;
            }
        }


        /// <summary>
        /// If name is null, throws an ArgumentNullException.
        /// 
        /// Otherwise, if name isn't a valid cell name, throws an InvalidNameException.
        /// 
        /// Otherwise, returns an enumeration, without duplicates, of the names of all cells whose
        /// values depend directly on the value of the named cell.  In other words, returns
        /// an enumeration, without duplicates, of the names of all cells that contain
        /// formulas containing name.
        /// 
        /// For example, suppose that
        /// A1 contains 3
        /// B1 contains the formula A1 * A1
        /// C1 contains the formula B1 + A1
        /// D1 contains the formula B1 - C1
        /// The direct dependents of A1 are B1 and C1
        /// </summary>
        protected override IEnumerable<String> GetDirectDependents(String name)
        {
            if (name == null)
            {
                throw new ArgumentNullException("inValid null name");
            }
            else if (!Regex.Match(name, @"^[a-zA-Z]{1}?[0-9]{1,2}?$").Success
                || !IsValid(name))
            {
                throw new InvalidNameException();
            }
            else
            {
                return Graph.GetDependents(Normalize(name));
            }
        }

        /// <summary>
        /// Takes in a Set of Formula cells that need to be reCalculated, 
        /// and reConstruct them
        /// </summary>
        /// <param name="Set">The Set of formulas to be reCalculated</param>
        private void Recalculate(ISet<string> Set)
        {
            IEnumerator<string> FormulastoCalculate = Set.GetEnumerator();
            FormulastoCalculate.MoveNext();
            while(FormulastoCalculate.MoveNext())
            {
                this.SetContentsOfCell(FormulastoCalculate.Current, 
                    Cells[FormulastoCalculate.Current].getCellContent().ToString());
            }
        }

        /// <summary>
        /// Check the version attribute of the given XML file through the file Path
        /// </summary>
        /// <param name="filePath">The path for the XML file</param>
        /// <returns>The version attribute</returns>
        private string versionChecking(string filePath)
        {
            XmlDocument xmlDoc = new XmlDocument();
            try { xmlDoc.Load(filePath); }
            catch (Exception) { throw new SpreadsheetReadWriteException("The file Path is not Valid: file was not found"); }

            XmlElement element = xmlDoc.DocumentElement;
            if (!element.HasAttribute("version"))
            { throw new SpreadsheetReadWriteException("The given XML File doesn't have version section/attribute"); }

            return element.GetAttribute("version");
        }
    }
}
