﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpreadsheetUtilities;

namespace SS
{
    /// <summary>
    /// A convenient class to represent a cell object
    /// </summary>
    /// <modifier>Fahad A Alothaimeen</modifier>
    /// <uNID>U0824563</uNID>
    public class Cell
    {
        private object Content;
        private object Value;
        private bool isEmpty;

        // A dictionary to be used to help linking the 
        // current collection of cells to the formula cell contructor
        private static Dictionary<String, Cell> CellsSoFar = new Dictionary<String, Cell>();

        /// <summary>
        /// Instantiate a new string cell
        /// </summary>
        /// <param name="_content">
        /// The string that will be created in the cell
        /// </param>
        public Cell(string _content)
        {
            Content = _content;
            Value = _content;
            if (_content == "")
            {
                isEmpty = true;
            }
            else
            {
                isEmpty = false;
            }
        }

        /// <summary>
        /// Instantiate a new double cell
        /// </summary>
        /// <param name="_content">
        /// The double that will be created in the cell
        /// </param>
        public Cell(double _content)
        {
            Content = _content;
            Value = _content;
            isEmpty = false;
        }

        /// <summary>
        /// Instantiate a new formula cell
        /// </summary>
        /// <param name="_content">
        /// The formula that will be created in the cell
        /// </param>
        /// <param name="Cells">
        /// The collection of the cells so far
        /// </param>
        public Cell(Formula _content, Dictionary<string, Cell> Cells)
        {
            Content = _content;
            CellsSoFar = Cells; // Link myHashTable to the Collection of cells so far
            Value = _content.Evaluate(this.testerMethod);
            isEmpty = false;
        }

        /// <summary>
        /// provides a convenient way to get the cells values so far 
        /// when a new formula cell is instantiated/requested
        /// </summary>
        /// <param name="cell">The name of the cell</param>
        /// <returns>The value of a given name of cell</returns>
        public double testerMethod(String cell)
        {

            if (CellsSoFar.ContainsKey(cell))
            {
                return (double)CellsSoFar[cell].getCellValue();
            }
            else
                throw new ArgumentException("The cell value does not exist");
        }

        /// <summary>
        /// Returns the a content of a cell
        /// </summary>
        /// <returns>The content of this cell</returns>
        public object getCellContent()
        {
            return Content;
        }

        /// <summary>
        /// Returns the a value of a cell
        /// </summary>
        /// <returns>The value of this cell</returns>
        public object getCellValue()
        {
            return Value;
        }

        /// <summary>
        /// Returns true, if the cell is empty
        /// false, otherwise
        /// </summary>
        /// <returns>true, if this cell is empty</returns>
        public bool isCellEmpty()
        {
            return isEmpty;
        }
    }
}
